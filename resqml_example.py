from parsers import create_epc_file_manifest, parse_epc_file_to_document_db, FileInfo

# For integration in airflow
# task_instance = context['task_instance']
# resqml_file = task_instance.xcom_pull('my_sensor_task', key='resqml_file')

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("file", help="EPC file to process")
args = parser.parse_args()

f = FileInfo(
    file_name = args.file,
    owners = ["data.test1@opendes.testing.com"],
    viewers = ["data.test1@opendes.testing.com"],
    legal_tags = ["opendes-manifest-default"],
    other_relevant_data_countries = ["US"],
    status = "compliant"
)

create_epc_file_manifest(f)

parse_epc_file_to_document_db(f)
