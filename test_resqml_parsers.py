import unittest

from parsers import FileInfo
from parsers.parser_factory import ParserFactory
from parsers.resqml_2_0.default_parser import DefaultResqmlParser20
from parsers.resqml_2_0.wellboretrajectoryrepresentation_parser import WellboreTrajectoryRepresentationParser20
from parsers.resqml_2_0.wellboreframerepresentation_parser import WellboreFrameRepresentationParser20
from parsers.resqml_2_0.wellboremarkerframerepresentation_parser import WellboreMarkerFrameRepresentationParser20
from parsers.resqml_2_0.wellboreinterpretation_parser import WellboreInterpretationParser20
from parsers.no_wpc_parser import NoWPCParser
from parsers.parser import Parser

from parsers.resqml_2_0.resqml_2_0_1_xsd import obj_PolylineSetRepresentation, obj_DiscreteProperty, obj_PropertyKind
from parsers.OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData

from parsers import create_epc_file_manifest

from collections import OrderedDict
import json
import os

from zipfile import ZipFile

file_name = ".//resqml_data//testingPackageCpp.epc"
group_name = "data.test1@opendes.testing.com"

class TestFactories(unittest.TestCase):

    def test_parser_factory(self):
        """
        Test the types are associated with factories
        """

        acl = AccessControlList(
            owners = [group_name],
            viewers = [group_name]
            
        )
        legal = LegalMetaData(
            legal_tags = ["opendes-manifest-default"],
            other_relevant_data_countries = ["US"],
            status = "compliant"
        )
        factory = ParserFactory(acl,legal)
        zip_file = ZipFile(open(file_name, 'rb'))
    
        # Declared types
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0",zip_file),DefaultResqmlParser20))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreTrajectoryRepresentation",zip_file),WellboreTrajectoryRepresentationParser20))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreFrameRepresentation",zip_file),WellboreFrameRepresentationParser20))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreMarkerFrameRepresentation",zip_file),WellboreMarkerFrameRepresentationParser20))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreInterpretation",zip_file),WellboreInterpretationParser20))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_CategoricalProperty",zip_file),NoWPCParser))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_ContinuousProperty",zip_file),NoWPCParser))
        self.assertTrue(isinstance(factory.get_parser("application/x-resqml+xml;version=2.0;type=obj_DiscreteProperty",zip_file),NoWPCParser))
        # Test default
        self.assertTrue(isinstance(factory.get_parser("AnyRandomType",zip_file),NoWPCParser))

    def test_parser(self):
        """
        Test the methods of the base parser
        """

        acl = AccessControlList(
            owners = [group_name],
            viewers = [group_name]
            
        )
        legal = LegalMetaData(
            legal_tags = ["opendes-manifest-default"],
            other_relevant_data_countries = ["US"],
            status = "compliant"
        )
        ParserFactory(acl,legal)
        zip_file = ZipFile(open(file_name, 'rb'))
        parser = Parser(zip_file,acl,legal)

        # testKey()
        self.assertEqual(parser.text_key(),"$")

        # remove_namespace()
        key = "{http://www.w3.org/2001/XMLSchema-instance}type"
        self.assertEqual(parser.remove_namespace(key),"type")
        pd = OrderedDict()

        # remove_namespaces()
        pd[key]="test"
        pd = parser.remove_namespaces(pd)
        self.assertEqual(pd["type"],"test")

        # get_element_text()
        pd = json.loads('{"element":{"$":"text"}}')
        self.assertEqual(parser.get_element_text(pd,"element"),"text")

        # get_dict_from_part()
        pkg = parser.zipfile()
        names = pkg.namelist()
        pd = parser.get_dict_from_part(names[3])
        self.assertEqual(pd["DiscreteProperty"]["Citation"]["Originator"]["$"],"Mathieu")

        # get_object_from_part()
        pkg = parser.zipfile()
        names = pkg.namelist()
        obj = parser.get_object_from_part(names[3],obj_DiscreteProperty)
        self.assertEqual(obj.Citation.Originator,"Mathieu")

        # get_associated_hdf5_file()
        pkg = parser.zipfile()
        names = pkg.namelist()
        obj = parser.get_object_from_part(names[3],obj_DiscreteProperty)
        hdf = parser.get_associated_hdf5_file(obj.PatchOfValues[0].Values.Values)
        self.assertEqual("testingPackageCpp.h5",hdf)
        
        # get_associated_objects_of_type()
        pkg = parser.zipfile()
        names = pkg.namelist()
        props = parser.get_associated_objects_of_type(names[3],obj_PropertyKind)
        self.assertEqual(len(props),1)
        self.assertTrue(isinstance(props[0],obj_PropertyKind))

        # get_dataset()
        pkg = parser.zipfile()
        names = pkg.namelist()
        obj = parser.get_object_from_part(names[3],obj_DiscreteProperty)
        dataset = parser.get_dataset(obj.PatchOfValues[0].Values.Values)
        self.assertEqual(dataset.ndim,3)

        # get_array_vector()
        pkg = parser.zipfile()
        names = pkg.namelist()
        obj = parser.get_object_from_part(names[24],obj_PolylineSetRepresentation)
        vec = parser.get_array_vector(obj.LinePatch[0].NodeCountPerPolyline)
        val= vec[0]
        self.assertEqual(val,3)

        # create_workproduct()
        wp = parser.create_workproduct(file_name)
        self.assertEqual(wp.data.name,os.path.basename(file_name))

        # create_file_part()
        file = parser.create_file_part(file_name)
        self.assertEqual(file.data.pre_load_file_path,file_name)

    def test__init(self):
        file = FileInfo(
            file_name = file_name,
            owners = [group_name],
            viewers = [group_name],
            legal_tags = ["opendes-manifest-default"],
            other_relevant_data_countries = ["US"],
            status = "compliant"
        )
        create_epc_file_manifest(file)

if __name__ == '__main__':
    unittest.main()