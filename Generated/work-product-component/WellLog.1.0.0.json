{
  "$id": "https://schema.osdu.opengroup.org/json/work-product-component/WellLog.1.0.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "WellLog",
  "description": "",
  "type": "object",
  "properties": {
    "ID": {
      "description": "The SRN which identifies this OSDU resource object without version.",
      "title": "Entity ID",
      "type": "string",
      "pattern": "^srn:<namespace>:work-product-component\\/WellLog:[^:]+$",
      "example": "srn:<namespace>:work-product-component/WellLog:d9ae27ff-8325-5b2a-b487-e42c9a87ce20"
    },
    "Kind": {
      "description": "The schema identification for the OSDU resource object following the pattern <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning scheme follows the semantic versioning, https://semver.org/.",
      "title": "Entity Kind",
      "type": "string",
      "pattern": "^[A-Za-z0-9-_]+:[A-Za-z0-9-_]+:[A-Za-z0-9-_]+:[0-9]+.[0-9]+.[0-9]+$",
      "example": "namespace:osdu:WellLog:2.7.112"
    },
    "GroupType": {
      "description": "The OSDU GroupType assigned to this resource object.",
      "title": "Group Type",
      "const": "work-product-component"
    },
    "Version": {
      "description": "The version number of this OSDU resource; set by the framework.",
      "title": "Version Number",
      "type": "number",
      "format": "int64",
      "example": 1831253916104085
    },
    "ACL": {
      "description": "The access control tags associated with this entity.",
      "title": "Access Control List",
      "$ref": "../abstract/AbstractAccessControlList.1.0.0.json"
    },
    "Legal": {
      "description": "The entity's legal tags and compliance status.",
      "title": "Legal Tags",
      "$ref": "../abstract/AbstractLegalTags.1.0.0.json"
    },
    "ResourceHomeRegionID": {
      "description": "The name of the home [cloud environment] region for this OSDU resource object.",
      "title": "Resource Home Region ID",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/OSDURegion:[^:]+:[0-9]*$"
    },
    "ResourceHostRegionIDs": {
      "description": "The name of the host [cloud environment] region(s) for this OSDU resource object.",
      "title": "Resource Host Region ID",
      "type": "array",
      "items": {
        "type": "string",
        "pattern": "^srn:<namespace>:reference-data\\/OSDURegion:[^:]+:[0-9]*$"
      }
    },
    "ResourceObjectCreationDateTime": {
      "description": "Timestamp of the time at which Version 1 of this OSDU resource object was originated.",
      "title": "Resource Object Creation DateTime",
      "type": "string",
      "format": "date-time"
    },
    "ResourceVersionCreationDateTime": {
      "description": "Timestamp of the time when the current version of this resource entered the OSDU.",
      "title": "Resource Version Creation DateTime",
      "type": "string",
      "format": "date-time"
    },
    "ResourceCurationStatus": {
      "description": "Describes the current Curation status.",
      "title": "Resource Curation Status",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceCurationStatus:[^:]+:[0-9]*$"
    },
    "ResourceLifecycleStatus": {
      "description": "Describes the current Resource Lifecycle status.",
      "title": "Resource Lifecycle Status",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceLifecycleStatus:[^:]+:[0-9]*$"
    },
    "ResourceSecurityClassification": {
      "description": "Classifies the security level of the resource.",
      "title": "Resource Security Classification",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceSecurityClassification:[^:]+:[0-9]*$"
    },
    "Ancestry": {
      "description": "The links to data, which constitute the inputs.",
      "title": "Ancestry",
      "$ref": "../abstract/AbstractLegalParentList.1.0.0.json"
    },
    "PersistableReferences": {
      "description": "The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions.",
      "title": "Frame of Reference Meta Data",
      "type": "array",
      "items": {
        "$ref": "../abstract/AbstractMetaItem.1.0.0.json"
      }
    },
    "Source": {
      "description": "Where did the data resource originate? This could be many kinds of entities, such as company, agency, team or individual.",
      "title": "Data Source",
      "type": "string",
      "pattern": "^srn:<namespace>:master-data\\/Organisation:[^:]+:[0-9]*$"
    },
    "ExistenceKind": {
      "description": "Where does this data resource sit in the cradle-to-grave span of its existence?",
      "title": "Existence Kind",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ExistenceKind:[^:]+:[0-9]*$"
    },
    "LicenseState": {
      "description": "Indicates what kind of ownership Company has over data.",
      "title": "License State",
      "pattern": "^srn:<namespace>:reference-data\\/LicenseState:[^:]+:[0-9]*$"
    },
    "Data": {
      "allOf": [
        {
          "$ref": "../abstract/AbstractWPCGroupType.1.0.0.json"
        },
        {
          "$ref": "../abstract/AbstractWorkProductComponent.1.0.0.json"
        },
        {
          "type": "object",
          "properties": {
            "WellboreID": {
              "type": "string",
              "pattern": "^srn:<namespace>:master-data\\/Wellbore:[^:]+:[0-9]*$",
              "description": "The Wellbore where the Well Log Work Product Component was recorded"
            },
            "WellLogTypeID": {
              "description": "Well Log Type short Description such as Raw; Evaluated; Composite;....",
              "type": "string",
              "pattern": "^srn:<namespace>:reference-data\\/LogType:[^:]+:[0-9]*$"
            },
            "TopMeasuredDepth": {
              "description": "OSDU Native Top Measured Depth of the Well Log - will be updated",
              "type": "object",
              "properties": {
                "Depth": {
                  "type": "number"
                },
                "UnitOfMeasure": {
                  "type": "string",
                  "pattern": "^srn:<namespace>:reference-data\\/UnitOfMeasure:[^:]+:[0-9]*$"
                }
              }
            },
            "BottomMeasuredDepth": {
              "description": "OSDU Native Bottom Measured Depth of the Well Log- will be updated",
              "type": "object",
              "properties": {
                "Depth": {
                  "type": "number"
                },
                "UnitOfMeasure": {
                  "type": "string",
                  "pattern": "^srn:<namespace>:reference-data\\/UnitOfMeasure:[^:]+:[0-9]*$"
                }
              }
            },
            "ServiceCompanyID": {
              "description": "Service Company ID",
              "type": "string",
              "pattern": "^srn:<namespace>:master-data\\/Organisation:[^:]+:[0-9]*$"
            },
            "LogSource": {
              "description": "OSDU Native Log Source - will be updated for later releases - not to be used yet ",
              "type": "string"
            },
            "LogActivity": {
              "description": "Log Activity, used to describe the type of pass such as Calibration Pass - Main Pass - Repeated Pass",
              "type": "string"
            },
            "LogRun": {
              "description": "Log Run - describe the run of the log - can be a number, but may be also a alphanumeric description such as a version name",
              "type": "string"
            },
            "LogVersion": {
              "description": "Log Version",
              "type": "string"
            },
            "LoggingService": {
              "description": "Logging Service - mainly a short concatenation of the names of the tools",
              "type": "string"
            },
            "LogServiceDateInterval": {
              "description": "An interval built from two nasted values : StartDate and EndDate. It applies to the whole log services and may apply to composite logs as [start of the first run job] and [end of the last run job]Log Service Date",
              "type": "object",
              "properties": {
                "StartDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "EndDate": {
                  "type": "string",
                  "format": "date-time"
                }
              }
            },
            "ToolStringDescription": {
              "description": "Tool String Description - a long concatenation of the tools used for logging services such as GammaRay+NeutronPorosity",
              "type": "string"
            },
            "LoggingDirection": {
              "description": "Specifies whether curves were collected downward or upward",
              "type": "string"
            },
            "PassNumber": {
              "description": "Indicates if the Pass is the Main one (1) or a repeated one - and it's level repetition",
              "type": "integer"
            },
            "ActivityType": {
              "description": "General method or circumstance of logging - MWD, completion, ...",
              "type": "string"
            },
            "DrillingFluidProperty": {
              "description": "Type of mud at time of logging (oil, water based,...)",
              "type": "string"
            },
            "HoleTypeLogging": {
              "description": "Description of the hole related type of logging - POSSIBLE VALUE : OpenHole / CasedHole / CementedHole",
              "pattern": "^OPENHOLE|CASEDHOLE|CEMENTEDHOLE$",
              "type": "string"
            },
            "Curves": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "CurveID": {
                    "description": "The ID of the Well Log Curve",
                    "type": "string"
                  },
                  "DateStamp": {
                    "description": "Date curve was created in the database",
                    "type": "string",
                    "format": "date-time"
                  },
                  "CurveVersion": {
                    "description": "The Version of the Log Curve.",
                    "type": "string"
                  },
                  "CurveQuality": {
                    "description": "The Quality of the Log Curve.",
                    "type": "string"
                  },
                  "InterpreterName": {
                    "description": "The name of person who interpreted this Log Curve.",
                    "type": "string"
                  },
                  "IsProcessed": {
                    "description": "Indicates if the curve has been (pre)processed or if it is a raw recording",
                    "type": "boolean"
                  },
                  "NullValue": {
                    "description": "Indicates that there is no measurement within the curve",
                    "type": "boolean"
                  },
                  "DepthCoding": {
                    "description": "The Coding of the depth.",
                    "type": "string",
                    "pattern": "^REGULAR|DISCRETE$"
                  },
                  "Interpolate": {
                    "description": "Whether curve can be interpolated or not",
                    "type": "boolean"
                  },
                  "TopDepth": {
                    "type": "number",
                    "description": "Top Depth"
                  },
                  "BaseDepth": {
                    "type": "number",
                    "description": "Base Depth"
                  },
                  "DepthUnit": {
                    "description": "Unit of Measure for Top and Base depth",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/UnitOfMeasure:[^:]+:[0-9]*$"
                  },
                  "CurveUnit": {
                    "description": "Unit of Measure for the Log Curve",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/UnitOfMeasure:[^:]+:[0-9]*$"
                  },
                  "Mnemonic": {
                    "description": "The Mnemonic of the Log Curve is the value as received either from Raw Providers or from Internal Processing team ",
                    "type": "string"
                  },
                  "LogCurveTypeID": {
                    "description": "The SRN of the Log Curve Type - which is the standard mnemonic chosen by the company - OSDU provides an initial list",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/LogCurveType:[^:]+:[0-9]*$"
                  },
                  "LogCurveBusinessValueID": {
                    "description": "The SRN of the Log Curve Business Value Type.",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/LogCurveBusinessValue:[^:]+:[0-9]*$"
                  },
                  "LogCurveMainFamilyID": {
                    "description": "The SRN of the Log Curve Main Family Type - which is the Geological Physical Quantity measured - such as porosity.",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/LogCurveMainFamily:[^:]+:[0-9]*$"
                  },
                  "LogCurveFamilyID": {
                    "description": "The SRN of the Log Curve Family - which is the detailed Geological Physical Quantity Measured - such as neutron porosity",
                    "type": "string",
                    "pattern": "^srn:<namespace>:reference-data\\/LogCurveFamily:[^:]+:[0-9]*$"
                  }
                }
              }
            }
          }
        },
        {
          "type": "object",
          "properties": {
            "ExtensionProperties": {
              "type": "object",
              "properties": {}
            }
          }
        }
      ]
    }
  },
  "required": [
    "ID",
    "Kind",
    "GroupType",
    "Version",
    "Legal",
    "ResourceObjectCreationDateTime",
    "ResourceVersionCreationDateTime"
  ],
  "additionalProperties": false
}