{
  "$id": "https://schema.osdu.opengroup.org/json/reference-data/SectionType.1.0.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "SectionType",
  "description": "Used to describe the type of sections.",
  "type": "object",
  "properties": {
    "ID": {
      "description": "The SRN which identifies this OSDU resource object without version.",
      "title": "Entity ID",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/SectionType:[^:]+$",
      "example": "srn:<namespace>:reference-data/SectionType:6940373c-8901-5b30-9b89-99d6667103e8"
    },
    "Kind": {
      "description": "The schema identification for the OSDU resource object following the pattern <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning scheme follows the semantic versioning, https://semver.org/.",
      "title": "Entity Kind",
      "type": "string",
      "pattern": "^[A-Za-z0-9-_]+:[A-Za-z0-9-_]+:[A-Za-z0-9-_]+:[0-9]+.[0-9]+.[0-9]+$",
      "example": "namespace:osdu:SectionType:2.7.112"
    },
    "GroupType": {
      "description": "The OSDU GroupType assigned to this resource object.",
      "title": "Group Type",
      "const": "reference-data"
    },
    "Version": {
      "description": "The version number of this OSDU resource; set by the framework.",
      "title": "Version Number",
      "type": "number",
      "format": "int64",
      "example": 1831253916104085
    },
    "ACL": {
      "description": "The access control tags associated with this entity.",
      "title": "Access Control List",
      "$ref": "../abstract/AbstractAccessControlList.1.0.0.json"
    },
    "Legal": {
      "description": "The entity's legal tags and compliance status.",
      "title": "Legal Tags",
      "$ref": "../abstract/AbstractLegalTags.1.0.0.json"
    },
    "ResourceHomeRegionID": {
      "description": "The name of the home [cloud environment] region for this OSDU resource object.",
      "title": "Resource Home Region ID",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/OSDURegion:[^:]+:[0-9]*$"
    },
    "ResourceHostRegionIDs": {
      "description": "The name of the host [cloud environment] region(s) for this OSDU resource object.",
      "title": "Resource Host Region ID",
      "type": "array",
      "items": {
        "type": "string",
        "pattern": "^srn:<namespace>:reference-data\\/OSDURegion:[^:]+:[0-9]*$"
      }
    },
    "ResourceObjectCreationDateTime": {
      "description": "Timestamp of the time at which Version 1 of this OSDU resource object was originated.",
      "title": "Resource Object Creation DateTime",
      "type": "string",
      "format": "date-time"
    },
    "ResourceVersionCreationDateTime": {
      "description": "Timestamp of the time when the current version of this resource entered the OSDU.",
      "title": "Resource Version Creation DateTime",
      "type": "string",
      "format": "date-time"
    },
    "ResourceCurationStatus": {
      "description": "Describes the current Curation status.",
      "title": "Resource Curation Status",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceCurationStatus:[^:]+:[0-9]*$"
    },
    "ResourceLifecycleStatus": {
      "description": "Describes the current Resource Lifecycle status.",
      "title": "Resource Lifecycle Status",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceLifecycleStatus:[^:]+:[0-9]*$"
    },
    "ResourceSecurityClassification": {
      "description": "Classifies the security level of the resource.",
      "title": "Resource Security Classification",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ResourceSecurityClassification:[^:]+:[0-9]*$"
    },
    "Ancestry": {
      "description": "The links to data, which constitute the inputs.",
      "title": "Ancestry",
      "$ref": "../abstract/AbstractLegalParentList.1.0.0.json"
    },
    "PersistableReferences": {
      "description": "The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions.",
      "title": "Frame of Reference Meta Data",
      "type": "array",
      "items": {
        "$ref": "../abstract/AbstractMetaItem.1.0.0.json"
      }
    },
    "Source": {
      "description": "Where did the data resource originate? This could be many kinds of entities, such as company, agency, team or individual.",
      "title": "Data Source",
      "type": "string",
      "pattern": "^srn:<namespace>:master-data\\/Organisation:[^:]+:[0-9]*$"
    },
    "ExistenceKind": {
      "description": "Where does this data resource sit in the cradle-to-grave span of its existence?",
      "title": "Existence Kind",
      "type": "string",
      "pattern": "^srn:<namespace>:reference-data\\/ExistenceKind:[^:]+:[0-9]*$"
    },
    "LicenseState": {
      "description": "Indicates what kind of ownership Company has over data.",
      "title": "License State",
      "pattern": "^srn:<namespace>:reference-data\\/LicenseState:[^:]+:[0-9]*$"
    },
    "Data": {
      "allOf": [
        {
          "$ref": "../abstract/AbstractReferenceType.1.0.0.json"
        },
        {
          "type": "object",
          "properties": {
            "ExtensionProperties": {
              "type": "object",
              "properties": {}
            }
          }
        }
      ]
    }
  },
  "required": [
    "ID",
    "Kind",
    "GroupType",
    "Version",
    "Legal",
    "ResourceObjectCreationDateTime",
    "ResourceVersionCreationDateTime"
  ],
  "additionalProperties": false
}