from ..parser import Parser
from collections import OrderedDict

import logging
log = logging.getLogger(__name__)

from .resqml_2_0_1_xsd import AbstractResqmlDataObject

from ..OSDU_types.Document_1_0_0 import Document, Data, LegalMetaData, AccessControlList

from datetime import datetime
from dateutil.parser import parse

from zipfile import ZipFile
from typing import Any, List

class DefaultResqmlParser20(Parser):
    """
    Parser adding Generic WPC from any EML object
    """
    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self, zip_file, acl, legal)

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        source_object = self.get_object_from_part(part_name,AbstractResqmlDataObject)

        source_uuid = source_object.uuid
        if source_uuid is None:
            return []
        
        creation, update = self.get_object_times(source_object)

        data = Data.from_dict(self.get_common_wpc_data_attributes(source_object))
        data.creation_date_time = creation
        data.date_modified = update
        source_type = source_object.extensiontype_.replace(source_object.ns_prefix_+':obj_','')
        data.document_subject='RESQML/' + source_type + '/' +source_uuid
        data.document_type_id='RESQML/' + source_type

        wpc = Document.from_dict(self.get_common_resource_attributes("work-product-component","Document",source_object.uuid, source_object))
        wpc.data = data
        return [wpc]
