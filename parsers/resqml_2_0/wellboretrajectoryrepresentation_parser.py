from ..parser import Parser
from ..OSDU_types.WellboreTrajectory_1_0_0 import WellboreTrajectory, Data, AbstractSpatialLocation as WellAbstractSpatialLocation
from ..OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from .resqml_2_0_1_xsd import obj_WellboreTrajectoryRepresentation, obj_MdDatum

from typing import List, Any

from zipfile import ZipFile

class WellboreTrajectoryRepresentationParser20(Parser):
    '''
    Parser adding WellboreTrajectory WPC fromm WellboreTrajectoryRepresentation
    '''
    def __init__(self, zip_file: ZipFile,acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self, zip_file, acl, legal)

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        source_object = self.get_object_from_part(part_name,obj_WellboreTrajectoryRepresentation)
        if source_object is None or not isinstance(source_object,obj_WellboreTrajectoryRepresentation):
            return []

        # Create WPC entry

        creation, update = self.get_object_times(source_object)

        data = Data.from_dict( self.get_common_wpc_data_attributes(source_object) )
        source_md_datum = self.get_referenced_object(source_object.MdDatum)
        if source_md_datum is not None and isinstance(source_md_datum, obj_MdDatum):
            location = self.get_location_from_datum_part( source_md_datum )
            if location:
                data.spatial_point = WellAbstractSpatialLocation.from_dict(location.to_dict())

        data.creation_date_time = creation
        data.top_depth_md=float(source_object.StartMd)
        data.top_depth_md_unit_id= "srn:reference-data/UnitOfMeasure:%s:" % source_object.MdUom
        data.wellbore_id="srn:master-data/Wellbore%s:" % source_object.RepresentedInterpretation.UUID

        wpc = WellboreTrajectory.from_dict(
            self.get_common_resource_attributes("work-product-component","WellTrajectory",source_object.uuid, source_object)
        )
        wpc.data = data

        return [wpc]
