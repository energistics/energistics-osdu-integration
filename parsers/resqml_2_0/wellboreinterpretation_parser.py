from ..parser import Parser

from .resqml_2_0_1_xsd import obj_WellboreInterpretation, obj_MdDatum
from ..OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from ..OSDU_types.Wellbore_1_0_0 import Wellbore, Data, AbstractSpatialLocation as WellAbstractSpatialLocation

from typing import Any, Optional

from typing import List

from zipfile import ZipFile

class WellboreInterpretationParser20(Parser):
    '''
    Parser adding Wellbore WPC fromm WellboreInterpretation
    '''
    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self,zip_file,acl,legal)

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        source_object = self.get_object_from_part(part_name,obj_WellboreInterpretation)

        if source_object.uuid is None:
            return []

        data = Data()

        source_md_datum_parts = self.get_associated_objects_of_type(part_name, obj_MdDatum)
        if len(source_md_datum_parts) > 0:
            source_md_datum = source_md_datum_parts[0]
            if source_md_datum is not None and isinstance(source_md_datum, obj_MdDatum):
                location = self.get_location_from_datum_part( source_md_datum )
                if location :
                    data.spatial_location = [ WellAbstractSpatialLocation.from_dict(location.to_dict()) ]

        wpc = Wellbore.from_dict(
            self.get_common_resource_attributes("work-product","Wellbore",source_object.uuid, source_object)
        )
        wpc.data = data

        return [wpc]
