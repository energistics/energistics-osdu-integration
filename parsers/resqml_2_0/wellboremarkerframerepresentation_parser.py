from ..parser import Parser
from .resqml_2_0_1_xsd import obj_WellboreMarkerFrameRepresentation, obj_MdDatum
from ..OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from ..OSDU_types.WellboreMarker_1_0_0 import WellboreMarker, Data, Marker, AbstractSpatialLocation as WellAbstractSpatialLocation

from typing import List, Any, Optional

from zipfile import ZipFile

class WellboreMarkerFrameRepresentationParser20(Parser):
    '''
    Parser adding WellboreMarker WPC fromm WellboreMarkerFrameRepresentation
    '''
    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self, zip_file,acl,legal)

    def get_markers_info(self, source_object: obj_WellboreMarkerFrameRepresentation) -> Optional[List[Marker]]:
        markers = None

        # Get measureDepth dataset
        source_md_dataset = None
        source_node_md = source_object.NodeMd
        source_md_dataset = self.get_array_vector(source_node_md)

        source_wellbore_markers = source_object.WellboreMarker
        if source_md_dataset is not None and len(source_wellbore_markers) != len(source_md_dataset):
            source_md_dataset = None

        # Build marker list
        m_count = 0
        for wm in source_wellbore_markers:
            wm_citation = wm.Citation
            if wm_citation is not None:
                marker_measured_depth = None
                if source_md_dataset is not None:
                    marker_measured_depth = source_md_dataset[m_count]
                m = Marker(
                    feature_name=None,
                    feature_type_id=None,
                    geological_age=None,
                    marker_date=None,
                    marker_interpreter=None,
                    marker_measured_depth=marker_measured_depth,
                    marker_name=wm_citation.Title,
                    marker_observation_number=None,
                    marker_type_id=None,
                    missing=None,
                    negative_vertical_delta=None,
                    positive_vertical_delta=None,
                    surface_dip_angle=None,
                    surface_dip_azimuth=None
                )
                if markers is not None:
                    markers += [m]
                else:
                    markers = [m]
                m_count += 1

        return markers

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        source_object = self.get_object_from_part(part_name,obj_WellboreMarkerFrameRepresentation)
        if source_object is None or not isinstance(source_object,obj_WellboreMarkerFrameRepresentation):
            return []

        data = Data.from_dict( self.get_common_wpc_data_attributes(source_object) )
        
        source_trajectory = self.get_referenced_object(source_object.Trajectory)
        if source_trajectory is not None:
            depth_unit = "srn:reference-data/UnitOfMeasure:" + source_trajectory.MdUom + ":"
            data.depth_unit=depth_unit

            # Get location and CRS from datum
            source_md_datum = self.get_referenced_object(source_trajectory.MdDatum)
            if source_md_datum is not None and isinstance(source_md_datum, obj_MdDatum):
                location = self.get_location_from_datum_part(
                    source_md_datum
                )
                if location :
                    data.spatial_point = WellAbstractSpatialLocation.from_dict(location.to_dict())

        source_wellbore = source_trajectory.RepresentedInterpretation
        if source_wellbore is not None:
            data.wellbore_id="srn:master-data/Wellbore:%s:" % source_object.RepresentedInterpretation.UUID

        wpc = WellboreMarker.from_dict(
            self.get_common_resource_attributes("work-product-component","WellboreMarker",source_object.uuid, source_object)
        )
        wpc.data = data

        return [wpc]
