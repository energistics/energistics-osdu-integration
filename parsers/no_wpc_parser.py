from .parser import Parser
from collections import OrderedDict
from zipfile import ZipFile

from typing import Tuple

from .OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from typing import Any, List

import logging
log = logging.getLogger(__name__)

class NoWPCParser(Parser):
    """
    Parser not adding any WPC
    """
    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self, zip_file, acl, legal)

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        return []
