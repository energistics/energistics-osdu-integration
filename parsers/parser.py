import h5py
import ntpath
import logging
from collections import OrderedDict
from zipfile import ZipFile
import re
from dateutil.parser import parse
import numpy
import os
import platform
import uuid

from typing import Dict

from lxml import etree
from xmljson import BadgerFish              # import the class
bf = BadgerFish(dict_type=OrderedDict)

log = logging.getLogger(__name__)

from .resqml_2_0.resqml_2_0_1_xsd import * # Import all classes so that object can be instanciated safely with most derived type
from .resqml_2_0.resqml_2_0_1_xsd import parsexmlstring_, GdsCollector_, get_root_tag # Not part of __all__

from .OSDU_types.WorkProduct_1_0_0 import WorkProduct, Data as WorkProductData, LegalMetaData as WorkProductLegalMetaData, AccessControlList as WorkProductAccessControlList

from .OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData, AbstractSpatialLocation, AbstractCoordinates
from .OSDU_types.File_1_0_0 import File, Data as FileData, LegalMetaData as FileLegalMetaData, AccessControlList as FileAccessControlList

from typing import Any, List, Optional, Tuple, Union, Type

from datetime import datetime


class Parser:
    """
    Base class for Parsers
    """
    namespace_pattern = re.compile(r'^{http:\/\/[^{}]+}')
    target_tag = '@Target'

    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        self._zf = zip_file
        self.acl = acl.to_dict()
        self.legal = legal.to_dict()

    @staticmethod
    def text_key():
        """
        Return text key in XML dict
        """
        return '$'

    @staticmethod
    def namespace():
        """
        Return OSDU namespace
        """
        return "opendes"

    @classmethod
    def remove_namespace(cls, key: str) -> str:
        """
        Remove XML namespace for a given element
        """
        key = re.sub(cls.namespace_pattern, '', key)
        return key

    @classmethod
    def remove_namespaces(cls, d):
        """
        Remove XML namespaces recursively from XML dict
        """
        if type(d) is OrderedDict:
            return OrderedDict([(cls.remove_namespace(k), cls.remove_namespaces(v)) for k, v in d.items()])
        elif type(d) is list:
            return list(map(cls.remove_namespaces, d))
        else:
            return d

    @staticmethod
    def get_element_text(xml_dict: OrderedDict, xml_key: str) -> Optional[str]:
        """
        Get the text from xml element and key
        """
        e = xml_dict.get(xml_key)
        if e is not None:
            return e.get(Parser.text_key(), None)
        return None

    def zipfile(self) ->ZipFile :
        """
        Get zipfile
        """
        return self._zf

    def get_dict_from_part(self, part_name: str, remove_name_space: bool = True) -> OrderedDict:
        """
        Get XML content for current part as a Python dictionary.
        Direct comversion without xml parsing
        """
        xml = bf.data(etree.fromstring(self._zf.read(part_name)))
        if remove_name_space:
            return self.remove_namespaces(xml)
        return xml

    def get_object_from_part(self, part_name: str, default_class) -> Type[AbstractResqmlDataObject]:
        """
        Get XML content for current part as a Python object.
        Default class indicates the class to use for parsing, can be a base XML class for the actual object.
        Provide type checking with xml schema parsing.
        Return the object and its type in a tuple.
        """
        xml = self._zf.read(part_name)

        root_node= parsexmlstring_(xml)
        gds_collector = GdsCollector_()
        root_tag, root_class = get_root_tag(root_node)
        if root_class is None:
            root_class = default_class
        doc = root_class.factory()
        doc.build(root_node, gds_collector_=gds_collector)
        return doc

    def get_associated_hdf5_file(self, dataset: Hdf5Dataset) -> Optional[str]:
        """
        Get HDF5 file referenced by the Hdf5Dataset XML element
        """
        source_hdf_proxy_uuid = dataset.HdfProxy.UUID
        source_hdf_proxy_rels = self.get_dict_from_part(
            "_rels/obj_EpcExternalPartReference_"+source_hdf_proxy_uuid+'.xml.rels')
        if source_hdf_proxy_rels is None:
            return None
        source_hdf_proxy_relationships = source_hdf_proxy_rels['Relationships']
        for rel in source_hdf_proxy_relationships['Relationship']:
            if rel['@Id'] == 'Hdf5File':
                return rel[self.target_tag]
        return None

    def get_associated_objects_of_type(self, current_part: str, part_type: Type[AbstractResqmlDataObject]) -> List[Type[AbstractResqmlDataObject]]:
        """
        Find the object of the given type through the relationships, relationship_type indicates the relation
        The default is @Target but @Origin can also be used
        """
        source_rels = self.get_dict_from_part('_rels/'+current_part+'.rels')
        if source_rels is None:
            return []
        source_relationships = source_rels['Relationships']
        targets = []
        for rel in source_relationships['Relationship']:
            if rel[self.target_tag].startswith(part_type.__name__+'_'):
                obj = self.get_object_from_part(rel[self.target_tag],part_type)
                targets += [obj]
        return targets

    def get_dataset(self, dataset: Hdf5Dataset) -> Optional[h5py.Dataset]:
        """
        Get the dataset contained in the Hdf5Dataset XML element
        """
        # Get file and path info
        hdf5file = self.get_associated_hdf5_file(dataset)
        if hdf5file is None:
            return None
        sources_path_in_hdf_file = dataset.PathInHdfFile
        # In case of relative path need to compute absolute
        if not os.path.isabs(hdf5file) and self._zf.filename is not None:
            d = os.path.dirname(self._zf.filename)
            if d is not None:
                hdf5file = os.path.join(d,hdf5file)

        if not os.path.exists(hdf5file):
            return None
            
            
        if sources_path_in_hdf_file is None:
            return None
        # Get the content from HDF5 file
        f = h5py.File(hdf5file, 'r')
        if f is None:
            return None
        return f[sources_path_in_hdf_file]

    def get_array_vector(self, array: Union[AbstractDoubleArray,AbstractIntegerArray]) -> Optional[Union[h5py.Dataset,numpy.ndarray]]:
        """
        Get a 1-dimensional vector from XML Arrays.
        Support HDF5, lattice and constant array for doubles and integers
        """
        if isinstance(array,DoubleHdf5Array) or isinstance(array,IntegerHdf5Array):
            return self.get_dataset(array.Values)
        elif isinstance(array,DoubleLatticeArray) or isinstance(array,IntegerLatticeArray):
            start = float(array.StartValue)
            offset = array.Offset[0]
            return numpy.arange(start,start+float(offset.Value)*(float(offset.Count)+1),float(offset.Value))
        elif isinstance(array,DoubleConstantArray) or isinstance(array,IntegerConstantArray):
            return numpy.full(array.Count,array.Value)
        return None

    def get_referenced_object(self, part_reference: Optional[DataObjectReference]) -> Optional[Type[AbstractResqmlDataObject]]:
        """
        Get the name and the type of a part referenced through a DataObjectReference
        """
        if part_reference is None:
            return None
        content_type = part_reference.ContentType
        content_type_tnd = content_type.find(";type=")
        if content_type_tnd == -1:
            return None
        object_type = content_type[content_type_tnd+6:]
        ref_part_id = object_type + '_' + part_reference.UUID +'.xml'
        return self.get_object_from_part(ref_part_id, eval(object_type))

    def create_workproduct(self, file_path: str) -> WorkProduct:
        """
        Create workProduct section from EPC file core properties
        """
        source_file_props = self.get_dict_from_part("docProps/core.xml")

        file_name = ntpath.basename(file_path)
        source_props = Parser.remove_namespaces(source_file_props)
        source_core_properties = source_props.get('coreProperties')
        aid = self.get_element_text(source_core_properties,"identifier")
        if aid is None:
            aid = str(uuid.uuid4())

        resource_object_creation_date_time = self.file_creation_date(file_path)
        resource_object_update_date_time = self.file_modification_date(file_path)

        wpd = WorkProductData(
            is_discoverable=True,
            is_extended_load=False,
            author_i_ds=[],
            business_activities=["Reservoir modeling"],
            creation_date_time=None,
            description=None,
            lineage_assertions=None,
            name=file_name,
            spatial_area=None,
            spatial_point=None,
            submitter_name=None,
            tags=None,
            extension_properties=None,
        )
        
        wp = WorkProduct.from_dict(self.get_common_resource_attributes("work-product","WorkProduct",aid, None))
        wp.data = wpd
        wp.resource_object_creation_date_time = resource_object_creation_date_time
        wp.resource_version_creation_date_time = resource_object_update_date_time
        return wp

    def create_file_part(self, file_path: str) -> File:
        """
        Create the file part of a manifest
        """
        source_file_props = self.get_dict_from_part("docProps/core.xml")

        file_name = ntpath.basename(file_path)
        source_props = Parser.remove_namespaces(source_file_props)
        source_core_properties = source_props.get('coreProperties')
        aid = self.get_element_text(source_core_properties,"identifier")
        if aid is None:
            aid = str(uuid.uuid4())

        resource_object_creation_date_time = self.file_creation_date(file_path)
        resource_object_update_date_time = self.file_modification_date(file_path)

        data = FileData(
            compression_method_type_id = "srn:%s:reference-data/CompressionMethodType:zip" % self.namespace(),
            file_size = os.path.getsize(file_path),
            pre_load_file_path = file_path,
            schema_format_type_id = "%s:osdu:resqml:2.0.1" % self.namespace()
        )

        file = File.from_dict(self.get_common_resource_attributes("file","File",aid, None))
        file.data = data
        file.resource_object_creation_date_time = resource_object_creation_date_time
        file.resource_version_creation_date_time = resource_object_update_date_time

        #TODO: Add associated HDF5
        return file

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        """
        Return Nothing 
        """
        return []

    @staticmethod
    def get_projected_crs(source_local_crs: AbstractLocal3dCrs ) -> Optional[str]:
        '''
        Get the OSDU id corresponding to projected CRS in RESQML CRS
        '''
        source_projected_crs = source_local_crs.ProjectedCrs
        if source_projected_crs and isinstance(source_projected_crs,ProjectedCrsEpsgCode) :
            source_epsg_code = source_projected_crs.EpsgCode
            # EPSG Code is not always there
            if source_epsg_code is not None:
                return 'srn:reference-data/HorizontalCRS:' + \
                    source_epsg_code+':'
        return None

    @staticmethod
    def get_vertical_crs(source_local_crs: AbstractLocal3dCrs ) -> Optional[str]:
        '''
        Get the OSDU id corresponding to vertical CRS in RESQML CRS
        '''
        source_vertical_crs = source_local_crs.VerticalCrs
        if source_vertical_crs  is not None and isinstance(source_vertical_crs,VerticalCrsEpsgCode) :
            source_epsg_code = source_vertical_crs.EpsgCode
            # EPSG Code is not always there
            if source_epsg_code is not None:
                return 'srn:reference-data/VerticalCRS:' + \
                    source_epsg_code+':'
        return None

    def create_location(self, x, y, z, crs: AbstractLocal3dCrs) -> Optional[AbstractSpatialLocation]:
        """
        Create location structure from coordinates and local crs
        """
        elevation = -1*float(z) if crs.ZIncreasingDownward else z
        spatial_location_coordinates_date = parse(crs.Citation.Creation)
        horizontal_crs_id = Parser.get_projected_crs(crs)
        vertical_crs_id = Parser.get_vertical_crs(crs)

        spatial_location = AbstractSpatialLocation(
            coordinate_quality_check_date_time=None,
            coordinate_quality_check_performed_by=None,
            coordinate_quality_check_remark=None,
            coordinates=[AbstractCoordinates( x=float(x), y=float(y) )],
            elevation=elevation,
            height_above_ground_level=None,
            height_above_ground_level_uomid=None,
            horizontal_crsid=horizontal_crs_id,
            qualitative_spatial_accuracy_type_id=None,
            quantitative_accuracy_band_id=None,
            spatial_geometry_type_id=None,
            spatial_location_coordinates_date=spatial_location_coordinates_date,
            spatial_parameter_type_id=None,
            vertical_crsid=vertical_crs_id
        )
        return spatial_location

    def get_location_from_datum_part(self, source_md_datum: obj_MdDatum) -> Optional[AbstractSpatialLocation]:
        '''
        Get the location information from Datum
        '''
        if source_md_datum is None:
            return None

        source_location = source_md_datum.Location
        if source_location is None:
            return None

        # Get CRS
        source_local_crs = self.get_referenced_object(source_md_datum.LocalCrs)
        if source_local_crs is None or not isinstance(source_local_crs,AbstractLocal3dCrs):
            return None
        return self.create_location(
            source_location.Coordinate1,source_location.Coordinate2,source_location.Coordinate3,source_local_crs
        )


    def get_common_resource_attributes(self, group_type, kind, uuid, source_object) -> Dict[str,Any]:
        '''
        Build a dictionary from the common attributes of resources
        '''
        now = datetime.now()
        creation, update = now, now
        if source_object:
            creation, update = self.get_object_times(source_object)
        doc_id = "srn:%s:%s/%s:%s" % (self.namespace(), group_type, kind, uuid)
        doc_kind = "%s:osdu:%s:1.0.0" % (self.namespace(),kind)
        return {
            "GroupType" : group_type,
            "ID" : doc_id,
            "Kind" : doc_kind,
            "LicenseState" : None,
            "Legal" : self.legal,
            "ACL": self.acl,
            "ExistenceKind" : None,
            "PersistableReferences" : None,
            "ResourceCurationStatus" : None,
            "ResourceHomeRegionID" : None,
            "ResourceHostRegionID" : [],
            "ResourceLifecycleStatus" : None,
            "ResourceSecurityClassification" : "srn:reference-data/ResourceSecurityClassification:RESTRICTED:",
            "ResourceObjectCreationDateTime" : creation.isoformat(),
            "ResourceVersionCreationDateTime" : update.isoformat(),
            "Version" : 1
        }

    def get_object_times(self, source_object)->Tuple[datetime, datetime] :
        '''
        Get object creation and update time
        '''
        resource_object_creation_date_time = parse(source_object.Citation.Creation)
        resource_object_update_date_time = resource_object_creation_date_time
        if source_object.Citation.LastUpdate is not None:
            resource_object_update_date_time = parse(source_object.Citation.LastUpdate)
        return resource_object_creation_date_time, resource_object_update_date_time

    def get_common_wpc_data_attributes(self, source_object) -> Dict[str,Any]:
        '''
        Build a dictionary from the common attributes of WPC data
        '''
        return {
            "Artefacts" : None,
            "Files" : None,
            "IsDiscoverable" : True,
            "IsExtendedLoad" : False,
            "AuthorID" : [source_object.Citation.Originator],
            "BusinessActivities" : None,
            "Description" : source_object.Citation.Description,
            "LineageAssertions" : None,
            "Name" : source_object.Citation.Title,
            "SubmitterName" : None,
            "Tags" : None,
            }
    
    @staticmethod
    def file_creation_date(path_to_file: str):
        '''
        Get file creation date
        '''
        return datetime.fromtimestamp(os.path.getctime(path_to_file))

    @staticmethod
    def file_modification_date(path_to_file: str):
        '''
        Get file modification date
        '''
        return datetime.fromtimestamp(os.path.getmtime(path_to_file))
