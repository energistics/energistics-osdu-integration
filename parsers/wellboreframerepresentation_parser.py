from .parser import Parser
from .resqml_2_0_1_xsd import obj_WellboreFrameRepresentation, obj_WellboreTrajectoryRepresentation, obj_MdDatum, \
    obj_ContinuousProperty, obj_DiscreteProperty, obj_CategoricalProperty, \
    AbstractValuesProperty, LocalPropertyKind, AbstractResqmlDataObject

from ..OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from ..OSDU_types.WellLog_1_0_0 import WellLog, Data, Curve, BottomMeasuredDepth, TopMeasuredDepth, AbstractSpatialLocation as WellAbstractSpatialLocation

from dateutil.parser import parse

from typing import List, Any, Optional, Type

import math

from zipfile import ZipFile


class WellboreFrameRepresentationParser20(Parser):
    """
    Parser adding WellLog WPC from WellboreFrameRepresentation
    """
    uom_srn = "srn:reference-data/UnitOfMeasure:"

    def __init__(self, zip_file: ZipFile, acl: AccessControlList, legal: LegalMetaData):
        Parser.__init__(self, zip_file,acl,legal)

    def get_curves(self, part_name: str, source_object: obj_WellboreFrameRepresentation) -> Optional[List[Curve]]:
        curves = None

        source_trajectory = self.get_referenced_object(source_object.Trajectory)
        if source_trajectory is not None:
            source_wellbore = source_trajectory.RepresentedInterpretation
            if source_wellbore is not None:
                depth_unit = "srn:reference-data/UnitOfMeasure:%s:" % source_trajectory.MdUom

        # Get measureDepth dataset
        source_node_md = source_object.NodeMd
        if source_node_md is None:
            return None

        source_md_dataset = self.get_array_vector(source_node_md)
        if source_md_dataset is None:
            return None

        properties: List[Type[AbstractResqmlDataObject]]  = []
        properties += self.get_associated_objects_of_type(part_name, obj_ContinuousProperty)
        properties += self.get_associated_objects_of_type(part_name, obj_DiscreteProperty)
        properties += self.get_associated_objects_of_type(part_name, obj_CategoricalProperty)

        for source_property in properties:

            base_depth = None
            top_depth = None
            if isinstance(source_property,obj_ContinuousProperty):
                curve_unit = self.uom_srn + source_property.UOM + ":"
            else:
                curve_unit = None

            if source_property.Citation.LastUpdate is None:
                resource_version_creation_date_time = parse(source_property.Citation.Creation)
            else:
                resource_version_creation_date_time = parse(source_property.Citation.LastUpdate)
                
            patches = source_property.PatchOfValues
            if not patches:
                continue
            
            patch = patches[0]
            if not patch:
                continue

            source_values_dataset = self.get_array_vector(patch.Values)
            if source_values_dataset is None or len(source_values_dataset)!=len(source_md_dataset):
                continue
                
            count = 0 
            for val in source_values_dataset:
                if not math.isnan(val):
                    md = source_md_dataset[count]
                    base_depth = md
                    if top_depth is None:
                        top_depth = md
                count += 1

            #TODO: propertType
            if isinstance(source_property.PropertyKind,LocalPropertyKind):
                log_curve_family_id = "srn:%s:work-product-component/Document:%s:" % (self.namespace(),source_property.PropertyKind.LocalPropertyKind.UUID)
            else:
                log_curve_family_id = "srn:%s:reference-data/PropertyNameType:%s" % (self.namespace(),source_property.PropertyKind.Kind)

            mnemonic = source_property.PropertyKind
            if isinstance(mnemonic, LocalPropertyKind):
                mnemonic = mnemonic.LocalPropertyKind.Title
            else:
                mnemonic = mnemonic.Kind

            curve= Curve(
                base_depth=base_depth,
                curve_id=source_property.uuid,
                curve_quality=None,
                curve_unit=curve_unit,
                curve_version=None,
                date_stamp=resource_version_creation_date_time,
                depth_coding=None,
                depth_unit=depth_unit,
                interpolate=None,
                interpreter_name=source_property.Citation.Originator,
                is_processed=None,
                log_curve_business_value_id=None,
                log_curve_family_id=None,
                log_curve_main_family_id=None,
                log_curve_type_id=log_curve_family_id,
                mnemonic=mnemonic,
                null_value=None,
                top_depth=top_depth
            )

            if curves is not None:
                curves += [curve]
            else:
                curves = [curve]
        return curves

    def add_workproduct_components(self, part_name: str) -> List[Any]:
        source_object = self.get_object_from_part(part_name,obj_WellboreFrameRepresentation)
        if source_object is None or not isinstance(source_object,obj_WellboreFrameRepresentation):
            return []

        data = Data.from_dict( self.get_common_wpc_data_attributes(source_object) )

        depth_unit: Optional[str] = None
        
        source_trajectory = self.get_referenced_object(source_object.Trajectory)
        if source_trajectory is not None:
            source_wellbore = source_trajectory.RepresentedInterpretation
            if source_wellbore is not None:
                data.wellbore_id="srn:master-data/Wellbore:%s:" % source_object.RepresentedInterpretation.UUID

            depth_unit = self.uom_srn + source_trajectory.MdUom + ":"

            # Get location and CRS from datum
            source_md_datum = self.get_referenced_object(source_trajectory.MdDatum)
            if source_md_datum is not None and isinstance(source_md_datum,obj_MdDatum):
                location = self.get_location_from_datum_part(source_md_datum)
                if location :
                    data.spatial_point = WellAbstractSpatialLocation.from_dict(location.to_dict())
        
        # Get measureDepth dataset
        source_md_dataset = self.get_array_vector(source_object.NodeMd)
        if source_md_dataset is not None:
            md_count = len(source_md_dataset)
            data.bottom_measured_depth = BottomMeasuredDepth(
                depth = source_md_dataset[md_count-1],
                unit_of_measure= depth_unit
            )
            data.top_measured_depth = TopMeasuredDepth(
                depth = source_md_dataset[0],
                unit_of_measure= depth_unit
            )

        data.curves=self.get_curves(part_name,source_object)

        wpc = WellLog.from_dict(self.get_common_resource_attributes("work-product-component","WellLog",source_object.uuid, source_object))
        wpc.data = data

        return [wpc]
