# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = seismic3_d_interpretation_project_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import List, Any, Optional, Dict, TypeVar, Callable, Type, cast
from datetime import datetime
from enum import Enum
import dateutil.parser


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return { k: f(v) for (k, v) in x.items() }


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


@dataclass
class AccessControlList:
    """The access control tags associated with this entity."""
    """The list of owners of this data record."""
    owners: List[str]
    """The list of viewers to which this data record is accessible/visible/discoverable."""
    viewers: List[str]

    @staticmethod
    def from_dict(obj: Any) -> 'AccessControlList':
        assert isinstance(obj, dict)
        owners = from_list(from_str, obj.get("Owners"))
        viewers = from_list(from_str, obj.get("Viewers"))
        return AccessControlList(owners, viewers)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Owners"] = from_list(from_str, self.owners)
        result["Viewers"] = from_list(from_str, self.viewers)
        return result


@dataclass
class ParentList:
    """The links to data, which constitute the inputs.
    
    A list of entity IDs in the data ecosystem, which act as legal parents to the current
    entity.
    """
    """An array of none, one or many entity references in the data ecosystem, which identify the
    source of data in the legal sense. Example: the 'parents' will be queried when e.g. the
    subscription of source data services is terminated; access to the derivatives is also
    terminated.
    """
    parents: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ParentList':
        assert isinstance(obj, dict)
        parents = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Parents"))
        return ParentList(parents)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Parents"] = from_union([lambda x: from_list(from_str, x), from_none], self.parents)
        return result


@dataclass
class Contractor:
    """Name of the team, unit, crew, party, or other subdivision of the Contractor that provided
    services.
    """
    contractor_crew: Optional[str] = None
    """Reference to a company that provided services."""
    contractor_organisation_id: Optional[str] = None
    """The identifier of a reference value for the role of a contractor providing services, such
    as Recording, Line Clearing, Positioning, Data Processing.
    """
    contractor_type_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Contractor':
        assert isinstance(obj, dict)
        contractor_crew = from_union([from_str, from_none], obj.get("ContractorCrew"))
        contractor_organisation_id = from_union([from_str, from_none], obj.get("ContractorOrganisationID"))
        contractor_type_id = from_union([from_str, from_none], obj.get("ContractorTypeID"))
        return Contractor(contractor_crew, contractor_organisation_id, contractor_type_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ContractorCrew"] = from_union([from_str, from_none], self.contractor_crew)
        result["ContractorOrganisationID"] = from_union([from_str, from_none], self.contractor_organisation_id)
        result["ContractorTypeID"] = from_union([from_str, from_none], self.contractor_type_id)
        return result


@dataclass
class FundsAuthorization:
    """Internal Company control number which identifies the allocation of funds to the Project."""
    authorization_id: Optional[str] = None
    """Type of currency for the authorized expenditure."""
    currency_id: Optional[str] = None
    """The date and time when the funds were approved."""
    effective_date_time: Optional[datetime] = None
    """The level of expenditure approved."""
    funds_amount: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FundsAuthorization':
        assert isinstance(obj, dict)
        authorization_id = from_union([from_str, from_none], obj.get("AuthorizationID"))
        currency_id = from_union([from_str, from_none], obj.get("CurrencyID"))
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        funds_amount = from_union([from_datetime, from_none], obj.get("FundsAmount"))
        return FundsAuthorization(authorization_id, currency_id, effective_date_time, funds_amount)

    def to_dict(self) -> dict:
        result: dict = {}
        result["AuthorizationID"] = from_union([from_str, from_none], self.authorization_id)
        result["CurrencyID"] = from_union([from_str, from_none], self.currency_id)
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["FundsAmount"] = from_union([lambda x: x.isoformat(), from_none], self.funds_amount)
        return result


@dataclass
class AbstractGeoContext:
    """A geographic context to an entity. It can be either a reference to a GeoPoliticalEntity,
    Basin, Field, Play or Prospect.
    
    A single, typed geo-political entity reference, which is 'abstracted' to
    AbstractGeoContext and then aggregated by GeoContexts properties.
    
    A single, typed basin entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed field entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed Play entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed Prospect entity reference, which is 'abstracted' to AbstractGeoContext
    and then aggregated by GeoContexts properties.
    """
    """The GeoPoliticalEntityType reference of the GeoPoliticalEntity (via GeoPoliticalEntityID)
    for application convenience.
    
    The BasinType reference of the Basin (via BasinID) for application convenience.
    
    The fixed type 'Field' for this AbstractGeoFieldContext.
    
    The PlayType reference of the Play (via PlayID) for application convenience.
    
    The ProspectType reference of the Prospect (via ProspectID) for application convenience.
    """
    geo_type_id: Any
    """Reference to GeoPoliticalEntity."""
    geo_political_entity_id: Optional[str] = None
    """Reference to Basin."""
    basin_id: Optional[str] = None
    """Reference to Field."""
    field_id: Optional[str] = None
    """Reference to the play."""
    play_id: Optional[str] = None
    """Reference to the prospect."""
    prospect_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractGeoContext':
        assert isinstance(obj, dict)
        geo_type_id = obj.get("GeoTypeID")
        geo_political_entity_id = from_union([from_str, from_none], obj.get("GeoPoliticalEntityID"))
        basin_id = from_union([from_str, from_none], obj.get("BasinID"))
        field_id = from_union([from_str, from_none], obj.get("FieldID"))
        play_id = from_union([from_str, from_none], obj.get("PlayID"))
        prospect_id = from_union([from_str, from_none], obj.get("ProspectID"))
        return AbstractGeoContext(geo_type_id, geo_political_entity_id, basin_id, field_id, play_id, prospect_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GeoTypeID"] = self.geo_type_id
        result["GeoPoliticalEntityID"] = from_union([from_str, from_none], self.geo_political_entity_id)
        result["BasinID"] = from_union([from_str, from_none], self.basin_id)
        result["FieldID"] = from_union([from_str, from_none], self.field_id)
        result["PlayID"] = from_union([from_str, from_none], self.play_id)
        result["ProspectID"] = from_union([from_str, from_none], self.prospect_id)
        return result


@dataclass
class Personnel:
    """Reference to the company which employs Personnel."""
    company_organisation_id: Optional[str] = None
    """Name of an individual supporting the Project."""
    person_name: Optional[str] = None
    """The identifier of a reference value for the role of an individual supporting a Project,
    such as Project Manager, Party Chief, Client Representative, Senior Observer.
    """
    project_role_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Personnel':
        assert isinstance(obj, dict)
        company_organisation_id = from_union([from_str, from_none], obj.get("CompanyOrganisationID"))
        person_name = from_union([from_str, from_none], obj.get("PersonName"))
        project_role_id = from_union([from_str, from_none], obj.get("ProjectRoleID"))
        return Personnel(company_organisation_id, person_name, project_role_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["CompanyOrganisationID"] = from_union([from_str, from_none], self.company_organisation_id)
        result["PersonName"] = from_union([from_str, from_none], self.person_name)
        result["ProjectRoleID"] = from_union([from_str, from_none], self.project_role_id)
        return result


@dataclass
class AbstractAliasNames:
    """A list of alternative names for an object.  The preferred name is in a separate, scalar
    property.  It may or may not be repeated in the alias list, though a best practice is to
    include it if the list is present, but to omit the list if there are no other names.
    Note that the abstract entity is an array so the $ref to it is a simple property
    reference.
    """
    """Alternative Name value of defined name type for an object."""
    alias_name: Optional[str] = None
    """A classification of alias names such as by role played or type of source, such as
    regulatory name, regulatory code, company code, international standard name, etc.
    """
    alias_name_type_id: Optional[str] = None
    """Organisation that provided the name (the source)."""
    definition_organisation_id: Optional[str] = None
    """The date and time when an alias name becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The data and time when an alias name is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractAliasNames':
        assert isinstance(obj, dict)
        alias_name = from_union([from_str, from_none], obj.get("AliasName"))
        alias_name_type_id = from_union([from_str, from_none], obj.get("AliasNameTypeID"))
        definition_organisation_id = from_union([from_str, from_none], obj.get("DefinitionOrganisationID"))
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return AbstractAliasNames(alias_name, alias_name_type_id, definition_organisation_id, effective_date_time, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["AliasName"] = from_union([from_str, from_none], self.alias_name)
        result["AliasNameTypeID"] = from_union([from_str, from_none], self.alias_name_type_id)
        result["DefinitionOrganisationID"] = from_union([from_str, from_none], self.definition_organisation_id)
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class ProjectSpecification:
    """The date and time at which a ProjectSpecification becomes effective."""
    effective_date_time: Optional[datetime] = None
    """Parameter type of property or characteristic."""
    parameter_type_id: Optional[str] = None
    """The actual date and time value of the parameter.  ISO format permits specification of
    time or date only.
    """
    project_specification_date_time: Optional[datetime] = None
    """The actual indicator value of the parameter."""
    project_specification_indicator: Optional[bool] = None
    """The value for the specified parameter type."""
    project_specification_quantity: Optional[float] = None
    """The actual text value of the parameter."""
    project_specification_text: Optional[str] = None
    """The date and time at which a ProjectSpecification is no longer in effect."""
    termination_date_time: Optional[datetime] = None
    """The unit for the quantity parameter if overriding the default for this ParameterType,
    like metre (m in SI units system) for quantity Length.
    """
    unit_of_measure_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ProjectSpecification':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        parameter_type_id = from_union([from_str, from_none], obj.get("ParameterTypeID"))
        project_specification_date_time = from_union([from_datetime, from_none], obj.get("ProjectSpecificationDateTime"))
        project_specification_indicator = from_union([from_bool, from_none], obj.get("ProjectSpecificationIndicator"))
        project_specification_quantity = from_union([from_float, from_none], obj.get("ProjectSpecificationQuantity"))
        project_specification_text = from_union([from_str, from_none], obj.get("ProjectSpecificationText"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        unit_of_measure_id = from_union([from_str, from_none], obj.get("UnitOfMeasureID"))
        return ProjectSpecification(effective_date_time, parameter_type_id, project_specification_date_time, project_specification_indicator, project_specification_quantity, project_specification_text, termination_date_time, unit_of_measure_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["ParameterTypeID"] = from_union([from_str, from_none], self.parameter_type_id)
        result["ProjectSpecificationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.project_specification_date_time)
        result["ProjectSpecificationIndicator"] = from_union([from_bool, from_none], self.project_specification_indicator)
        result["ProjectSpecificationQuantity"] = from_union([to_float, from_none], self.project_specification_quantity)
        result["ProjectSpecificationText"] = from_union([from_str, from_none], self.project_specification_text)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        result["UnitOfMeasureID"] = from_union([from_str, from_none], self.unit_of_measure_id)
        return result


@dataclass
class ProjectState:
    """The date and time at which the state becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The Project life cycle state from planning to completion."""
    project_state_type_id: Optional[str] = None
    """The date and time at which the state is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ProjectState':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        project_state_type_id = from_union([from_str, from_none], obj.get("ProjectStateTypeID"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return ProjectState(effective_date_time, project_state_type_id, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["ProjectStateTypeID"] = from_union([from_str, from_none], self.project_state_type_id)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class AbstractCoordinates:
    """A geographic position on the surface of the earth."""
    """x is Easting or Longitude."""
    x: Optional[float] = None
    """y is Northing or Latitude."""
    y: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractCoordinates':
        assert isinstance(obj, dict)
        x = from_union([from_float, from_none], obj.get("x"))
        y = from_union([from_float, from_none], obj.get("y"))
        return AbstractCoordinates(x, y)

    def to_dict(self) -> dict:
        result: dict = {}
        result["x"] = from_union([to_float, from_none], self.x)
        result["y"] = from_union([to_float, from_none], self.y)
        return result


@dataclass
class AbstractSpatialLocation:
    """A spatial geometry object that indicates the area of operation or interest for a
    Project.
    
    A geographic object which can be described by a set of points.
    """
    """The date of the Quality Check."""
    coordinate_quality_check_date_time: Optional[datetime] = None
    """The user who performed the Quality Check."""
    coordinate_quality_check_performed_by: Optional[str] = None
    """Freetext remark on Quality Check."""
    coordinate_quality_check_remark: Optional[str] = None
    """Projected or geographic coordinates."""
    coordinates: Optional[List[AbstractCoordinates]] = None
    """The elevation of the measured coordinates above the datum expressed by the VerticalCRS."""
    elevation: Optional[float] = None
    """The height of the measured coordinates above the ground."""
    height_above_ground_level: Optional[float] = None
    """Unit of Measure for the height above ground level."""
    height_above_ground_level_uomid: Optional[str] = None
    """Horizontal CRS."""
    horizontal_crsid: Optional[str] = None
    """A qualitative description of the quality of a spatial location, e.g. unverifiable, not
    verified, basic validation.
    """
    qualitative_spatial_accuracy_type_id: Optional[str] = None
    """An approximate quantitative assessment of the quality of a location (accurate to > 500 m
    (i.e. not very accurate)), to < 1 m, etc.
    """
    quantitative_accuracy_band_id: Optional[str] = None
    """Indicates the expected look of the SPATIAL_PARAMETER_TYPE, e.g. a point, a line, a
    polyline (e.g. coastline, made up of vertexes), an area, a volume. E.g. a Well Surface is
    a point (an identifiable feature in its own right), a coastline could be a polyline.
    """
    spatial_geometry_type_id: Optional[str] = None
    """Date when coordinates were measured or retrieved."""
    spatial_location_coordinates_date: Optional[datetime] = None
    """A type of spatial representation of an object, often general (e.g. an Outline, which
    could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore
    Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that
    may be used by Countries).
    """
    spatial_parameter_type_id: Optional[str] = None
    """Vertical CRS."""
    vertical_crsid: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractSpatialLocation':
        assert isinstance(obj, dict)
        coordinate_quality_check_date_time = from_union([from_datetime, from_none], obj.get("CoordinateQualityCheckDateTime"))
        coordinate_quality_check_performed_by = from_union([from_str, from_none], obj.get("CoordinateQualityCheckPerformedBy"))
        coordinate_quality_check_remark = from_union([from_str, from_none], obj.get("CoordinateQualityCheckRemark"))
        coordinates = from_union([lambda x: from_list(AbstractCoordinates.from_dict, x), from_none], obj.get("Coordinates"))
        elevation = from_union([from_float, from_none], obj.get("Elevation"))
        height_above_ground_level = from_union([from_float, from_none], obj.get("HeightAboveGroundLevel"))
        height_above_ground_level_uomid = from_union([from_str, from_none], obj.get("HeightAboveGroundLevelUOMID"))
        horizontal_crsid = from_union([from_str, from_none], obj.get("HorizontalCRSID"))
        qualitative_spatial_accuracy_type_id = from_union([from_str, from_none], obj.get("QualitativeSpatialAccuracyTypeID"))
        quantitative_accuracy_band_id = from_union([from_str, from_none], obj.get("QuantitativeAccuracyBandID"))
        spatial_geometry_type_id = from_union([from_str, from_none], obj.get("SpatialGeometryTypeID"))
        spatial_location_coordinates_date = from_union([from_datetime, from_none], obj.get("SpatialLocationCoordinatesDate"))
        spatial_parameter_type_id = from_union([from_str, from_none], obj.get("SpatialParameterTypeID"))
        vertical_crsid = from_union([from_str, from_none], obj.get("VerticalCRSID"))
        return AbstractSpatialLocation(coordinate_quality_check_date_time, coordinate_quality_check_performed_by, coordinate_quality_check_remark, coordinates, elevation, height_above_ground_level, height_above_ground_level_uomid, horizontal_crsid, qualitative_spatial_accuracy_type_id, quantitative_accuracy_band_id, spatial_geometry_type_id, spatial_location_coordinates_date, spatial_parameter_type_id, vertical_crsid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["CoordinateQualityCheckDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.coordinate_quality_check_date_time)
        result["CoordinateQualityCheckPerformedBy"] = from_union([from_str, from_none], self.coordinate_quality_check_performed_by)
        result["CoordinateQualityCheckRemark"] = from_union([from_str, from_none], self.coordinate_quality_check_remark)
        result["Coordinates"] = from_union([lambda x: from_list(lambda x: to_class(AbstractCoordinates, x), x), from_none], self.coordinates)
        result["Elevation"] = from_union([to_float, from_none], self.elevation)
        result["HeightAboveGroundLevel"] = from_union([to_float, from_none], self.height_above_ground_level)
        result["HeightAboveGroundLevelUOMID"] = from_union([from_str, from_none], self.height_above_ground_level_uomid)
        result["HorizontalCRSID"] = from_union([from_str, from_none], self.horizontal_crsid)
        result["QualitativeSpatialAccuracyTypeID"] = from_union([from_str, from_none], self.qualitative_spatial_accuracy_type_id)
        result["QuantitativeAccuracyBandID"] = from_union([from_str, from_none], self.quantitative_accuracy_band_id)
        result["SpatialGeometryTypeID"] = from_union([from_str, from_none], self.spatial_geometry_type_id)
        result["SpatialLocationCoordinatesDate"] = from_union([lambda x: x.isoformat(), from_none], self.spatial_location_coordinates_date)
        result["SpatialParameterTypeID"] = from_union([from_str, from_none], self.spatial_parameter_type_id)
        result["VerticalCRSID"] = from_union([from_str, from_none], self.vertical_crsid)
        return result


@dataclass
class Data:
    """A Project is a business activity that consumes financial and human resources and produces
    (digital) work products.
    """
    """References to applicable agreements in external contract database system of record."""
    contract_i_ds: Optional[List[str]] = None
    """References to organisations which supplied services to the Project."""
    contractors: Optional[List[Contractor]] = None
    """The history of expenditure approvals."""
    funds_authorization: Optional[List[FundsAuthorization]] = None
    """List of geographic entities which provide context to the project.  This may include
    multiple types or multiple values of the same type.
    """
    geo_contexts: Optional[List[AbstractGeoContext]] = None
    """The organisation which controlled the conduct of the project."""
    operator: Optional[str] = None
    """List of key individuals supporting the Project.  This could be Abstracted for re-use, and
    could reference a separate Persons master data object.
    """
    personnel: Optional[List[Personnel]] = None
    """The date and time when the Project was initiated."""
    project_begin_date: Optional[datetime] = None
    """The date and time when the Project was completed."""
    project_end_date: Optional[datetime] = None
    """A system-specified unique identifier of a Project."""
    project_id: Optional[str] = None
    """The common or preferred name of a Project."""
    project_name: Optional[str] = None
    """The history of Project names, codes, and other business identifiers."""
    project_names: Optional[List[AbstractAliasNames]] = None
    """General parameters defining the configuration of the Project.  In the case of a seismic
    acquisition project it is like receiver interval, source depth, source type.  In the case
    of a processing project, it is like replacement velocity, reference datum above mean sea
    level.
    """
    project_specification: Optional[List[ProjectSpecification]] = None
    """The history of life cycle states that the Project has been through.."""
    project_state: Optional[List[ProjectState]] = None
    """The identifier of a reference value for a kind of business project, such as Seismic
    Acquisition, Seismic Processing, Seismic Interpretation.
    """
    project_type_id: Optional[str] = None
    """Description of the objectives of a Project."""
    purpose: Optional[str] = None
    """A spatial geometry object that indicates the area of operation or interest for a Project."""
    spatial_location: Optional[AbstractSpatialLocation] = None
    """A reference to the Bin Grid that all the associated traces and horizons are based on."""
    seismic_bin_grid_id: Optional[str] = None
    extension_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Data':
        assert isinstance(obj, dict)
        contract_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("ContractIDs"))
        contractors = from_union([lambda x: from_list(Contractor.from_dict, x), from_none], obj.get("Contractors"))
        funds_authorization = from_union([lambda x: from_list(FundsAuthorization.from_dict, x), from_none], obj.get("FundsAuthorization"))
        geo_contexts = from_union([lambda x: from_list(AbstractGeoContext.from_dict, x), from_none], obj.get("GeoContexts"))
        operator = from_union([from_str, from_none], obj.get("Operator"))
        personnel = from_union([lambda x: from_list(Personnel.from_dict, x), from_none], obj.get("Personnel"))
        project_begin_date = from_union([from_datetime, from_none], obj.get("ProjectBeginDate"))
        project_end_date = from_union([from_datetime, from_none], obj.get("ProjectEndDate"))
        project_id = from_union([from_str, from_none], obj.get("ProjectID"))
        project_name = from_union([from_str, from_none], obj.get("ProjectName"))
        project_names = from_union([lambda x: from_list(AbstractAliasNames.from_dict, x), from_none], obj.get("ProjectNames"))
        project_specification = from_union([lambda x: from_list(ProjectSpecification.from_dict, x), from_none], obj.get("ProjectSpecification"))
        project_state = from_union([lambda x: from_list(ProjectState.from_dict, x), from_none], obj.get("ProjectState"))
        project_type_id = from_union([from_str, from_none], obj.get("ProjectTypeID"))
        purpose = from_union([from_str, from_none], obj.get("Purpose"))
        spatial_location = from_union([AbstractSpatialLocation.from_dict, from_none], obj.get("SpatialLocation"))
        seismic_bin_grid_id = from_union([from_str, from_none], obj.get("SeismicBinGridID"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        return Data(contract_i_ds, contractors, funds_authorization, geo_contexts, operator, personnel, project_begin_date, project_end_date, project_id, project_name, project_names, project_specification, project_state, project_type_id, purpose, spatial_location, seismic_bin_grid_id, extension_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ContractIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.contract_i_ds)
        result["Contractors"] = from_union([lambda x: from_list(lambda x: to_class(Contractor, x), x), from_none], self.contractors)
        result["FundsAuthorization"] = from_union([lambda x: from_list(lambda x: to_class(FundsAuthorization, x), x), from_none], self.funds_authorization)
        result["GeoContexts"] = from_union([lambda x: from_list(lambda x: to_class(AbstractGeoContext, x), x), from_none], self.geo_contexts)
        result["Operator"] = from_union([from_str, from_none], self.operator)
        result["Personnel"] = from_union([lambda x: from_list(lambda x: to_class(Personnel, x), x), from_none], self.personnel)
        result["ProjectBeginDate"] = from_union([lambda x: x.isoformat(), from_none], self.project_begin_date)
        result["ProjectEndDate"] = from_union([lambda x: x.isoformat(), from_none], self.project_end_date)
        result["ProjectID"] = from_union([from_str, from_none], self.project_id)
        result["ProjectName"] = from_union([from_str, from_none], self.project_name)
        result["ProjectNames"] = from_union([lambda x: from_list(lambda x: to_class(AbstractAliasNames, x), x), from_none], self.project_names)
        result["ProjectSpecification"] = from_union([lambda x: from_list(lambda x: to_class(ProjectSpecification, x), x), from_none], self.project_specification)
        result["ProjectState"] = from_union([lambda x: from_list(lambda x: to_class(ProjectState, x), x), from_none], self.project_state)
        result["ProjectTypeID"] = from_union([from_str, from_none], self.project_type_id)
        result["Purpose"] = from_union([from_str, from_none], self.purpose)
        result["SpatialLocation"] = from_union([lambda x: to_class(AbstractSpatialLocation, x), from_none], self.spatial_location)
        result["SeismicBinGridID"] = from_union([from_str, from_none], self.seismic_bin_grid_id)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        return result


@dataclass
class LegalMetaData:
    """The entity's legal tags and compliance status.
    
    Legal meta data like legal tags, relevant other countries, legal status.
    """
    """The list of legal tags."""
    legal_tags: List[str]
    """The list of other relevant data countries."""
    other_relevant_data_countries: List[str]
    """The legal status."""
    status: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LegalMetaData':
        assert isinstance(obj, dict)
        legal_tags = from_list(from_str, obj.get("LegalTags"))
        other_relevant_data_countries = from_list(from_str, obj.get("OtherRelevantDataCountries"))
        status = from_union([from_str, from_none], obj.get("Status"))
        return LegalMetaData(legal_tags, other_relevant_data_countries, status)

    def to_dict(self) -> dict:
        result: dict = {}
        result["LegalTags"] = from_list(from_str, self.legal_tags)
        result["OtherRelevantDataCountries"] = from_list(from_str, self.other_relevant_data_countries)
        result["Status"] = from_union([from_str, from_none], self.status)
        return result


class ReferenceKind(Enum):
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    AZIMUTH_REFERENCE = "AzimuthReference"
    CRS = "CRS"
    DATE_TIME = "DateTime"
    MEASUREMENT = "Measurement"
    UNIT = "Unit"


@dataclass
class FrameOfReferenceMetaDataItem:
    """A meta data item, which allows the association of named properties or property values to
    a Unit/Measurement/CRS/Azimuth/Time context.
    """
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    kind: ReferenceKind
    """The persistable reference string uniquely identifying the CRS or Unit."""
    persistable_reference: str
    """The name of the CRS or the symbol/name of the unit."""
    name: Optional[str] = None
    """The list of property names, to which this meta data item provides Unit/CRS context to.
    Data structures, which come in a single frame of reference, can register the property
    name, others require a full path like "data.structureA.propertyB" to define a unique
    context.
    """
    property_names: Optional[List[str]] = None
    """The list of property values, to which this meta data item provides Unit/CRS context to.
    Typically a unit symbol is a value to a data structure; this symbol is then registered in
    this propertyValues array and the persistableReference provides the absolute reference.
    """
    property_values: Optional[List[str]] = None
    """The uncertainty of the values measured given the unit or CRS unit."""
    uncertainty: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FrameOfReferenceMetaDataItem':
        assert isinstance(obj, dict)
        kind = ReferenceKind(obj.get("Kind"))
        persistable_reference = from_str(obj.get("PersistableReference"))
        name = from_union([from_str, from_none], obj.get("Name"))
        property_names = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyNames"))
        property_values = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyValues"))
        uncertainty = from_union([from_float, from_none], obj.get("Uncertainty"))
        return FrameOfReferenceMetaDataItem(kind, persistable_reference, name, property_names, property_values, uncertainty)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Kind"] = to_enum(ReferenceKind, self.kind)
        result["PersistableReference"] = from_str(self.persistable_reference)
        result["Name"] = from_union([from_str, from_none], self.name)
        result["PropertyNames"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_names)
        result["PropertyValues"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_values)
        result["Uncertainty"] = from_union([to_float, from_none], self.uncertainty)
        return result


@dataclass
class Seismic3DInterpretationProject:
    """A collection of trace datasets with a common bin grid that are important for
    interpretation.  Trace datasets and Horizons are associated through Lineage, although a
    master collection of trace datasets are explicitly related through a child relationship.
    It is not all of the datasets with a common bin grid, or all of the datasets from a
    processing project, or all of the datasets from an acquisition project because some are
    not suitable for interpretation.  It can be thought of a project as a master or
    authorized project but it is not an application project as the term project is typically
    used within interpretation systems.  Interpretation objects (horizons) are hung from an
    interpretation project to give context and to interpret spatial location.
    """
    """The OSDU GroupType assigned to this resource object."""
    group_type: Any
    """The SRN which identifies this OSDU resource object without version."""
    id: str
    """The schema identification for the OSDU resource object following the pattern
    <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning
    scheme follows the semantic versioning, https://semver.org/.
    """
    kind: str
    """The entity's legal tags and compliance status."""
    legal: LegalMetaData
    """Indicates what kind of ownership Company has over data."""
    license_state: Any
    """Timestamp of the time at which Version 1 of this OSDU resource object was originated."""
    resource_object_creation_date_time: datetime
    """Timestamp of the time when the current version of this resource entered the OSDU."""
    resource_version_creation_date_time: datetime
    """The version number of this OSDU resource; set by the framework."""
    version: float
    """The access control tags associated with this entity."""
    acl: Optional[AccessControlList] = None
    """The links to data, which constitute the inputs."""
    ancestry: Optional[ParentList] = None
    data: Optional[Data] = None
    """Where does this data resource sit in the cradle-to-grave span of its existence?"""
    existence_kind: Optional[str] = None
    """The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions."""
    persistable_references: Optional[List[FrameOfReferenceMetaDataItem]] = None
    """Describes the current Curation status."""
    resource_curation_status: Optional[str] = None
    """The name of the home [cloud environment] region for this OSDU resource object."""
    resource_home_region_id: Optional[str] = None
    """The name of the host [cloud environment] region(s) for this OSDU resource object."""
    resource_host_region_i_ds: Optional[List[str]] = None
    """Describes the current Resource Lifecycle status."""
    resource_lifecycle_status: Optional[str] = None
    """Classifies the security level of the resource."""
    resource_security_classification: Optional[str] = None
    """Where did the data resource originate? This could be many kinds of entities, such as
    company, agency, team or individual.
    """
    source: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Seismic3DInterpretationProject':
        assert isinstance(obj, dict)
        group_type = obj.get("GroupType")
        id = from_str(obj.get("ID"))
        kind = from_str(obj.get("Kind"))
        legal = LegalMetaData.from_dict(obj.get("Legal"))
        license_state = obj.get("LicenseState")
        resource_object_creation_date_time = from_datetime(obj.get("ResourceObjectCreationDateTime"))
        resource_version_creation_date_time = from_datetime(obj.get("ResourceVersionCreationDateTime"))
        version = from_float(obj.get("Version"))
        acl = from_union([AccessControlList.from_dict, from_none], obj.get("ACL"))
        ancestry = from_union([ParentList.from_dict, from_none], obj.get("Ancestry"))
        data = from_union([Data.from_dict, from_none], obj.get("Data"))
        existence_kind = from_union([from_str, from_none], obj.get("ExistenceKind"))
        persistable_references = from_union([lambda x: from_list(FrameOfReferenceMetaDataItem.from_dict, x), from_none], obj.get("PersistableReferences"))
        resource_curation_status = from_union([from_str, from_none], obj.get("ResourceCurationStatus"))
        resource_home_region_id = from_union([from_str, from_none], obj.get("ResourceHomeRegionID"))
        resource_host_region_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("ResourceHostRegionIDs"))
        resource_lifecycle_status = from_union([from_str, from_none], obj.get("ResourceLifecycleStatus"))
        resource_security_classification = from_union([from_str, from_none], obj.get("ResourceSecurityClassification"))
        source = from_union([from_str, from_none], obj.get("Source"))
        return Seismic3DInterpretationProject(group_type, id, kind, legal, license_state, resource_object_creation_date_time, resource_version_creation_date_time, version, acl, ancestry, data, existence_kind, persistable_references, resource_curation_status, resource_home_region_id, resource_host_region_i_ds, resource_lifecycle_status, resource_security_classification, source)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GroupType"] = self.group_type
        result["ID"] = from_str(self.id)
        result["Kind"] = from_str(self.kind)
        result["Legal"] = to_class(LegalMetaData, self.legal)
        result["LicenseState"] = self.license_state
        result["ResourceObjectCreationDateTime"] = self.resource_object_creation_date_time.isoformat()
        result["ResourceVersionCreationDateTime"] = self.resource_version_creation_date_time.isoformat()
        result["Version"] = to_float(self.version)
        result["ACL"] = from_union([lambda x: to_class(AccessControlList, x), from_none], self.acl)
        result["Ancestry"] = from_union([lambda x: to_class(ParentList, x), from_none], self.ancestry)
        result["Data"] = from_union([lambda x: to_class(Data, x), from_none], self.data)
        result["ExistenceKind"] = from_union([from_str, from_none], self.existence_kind)
        result["PersistableReferences"] = from_union([lambda x: from_list(lambda x: to_class(FrameOfReferenceMetaDataItem, x), x), from_none], self.persistable_references)
        result["ResourceCurationStatus"] = from_union([from_str, from_none], self.resource_curation_status)
        result["ResourceHomeRegionID"] = from_union([from_str, from_none], self.resource_home_region_id)
        result["ResourceHostRegionIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.resource_host_region_i_ds)
        result["ResourceLifecycleStatus"] = from_union([from_str, from_none], self.resource_lifecycle_status)
        result["ResourceSecurityClassification"] = from_union([from_str, from_none], self.resource_security_classification)
        result["Source"] = from_union([from_str, from_none], self.source)
        return result


def seismic3_d_interpretation_project_from_dict(s: Any) -> Seismic3DInterpretationProject:
    return Seismic3DInterpretationProject.from_dict(s)


def seismic3_d_interpretation_project_to_dict(x: Seismic3DInterpretationProject) -> Any:
    return to_class(Seismic3DInterpretationProject, x)
