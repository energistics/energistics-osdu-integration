# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = work_product_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import List, Any, Optional, Dict, TypeVar, Callable, Type, cast
from datetime import datetime
from enum import Enum
import dateutil.parser


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return { k: f(v) for (k, v) in x.items() }


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


@dataclass
class AccessControlList:
    """The access control tags associated with this entity."""
    """The list of owners of this data record."""
    owners: List[str]
    """The list of viewers to which this data record is accessible/visible/discoverable."""
    viewers: List[str]

    @staticmethod
    def from_dict(obj: Any) -> 'AccessControlList':
        assert isinstance(obj, dict)
        owners = from_list(from_str, obj.get("Owners"))
        viewers = from_list(from_str, obj.get("Viewers"))
        return AccessControlList(owners, viewers)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Owners"] = from_list(from_str, self.owners)
        result["Viewers"] = from_list(from_str, self.viewers)
        return result


@dataclass
class ParentList:
    """The links to data, which constitute the inputs.
    
    A list of entity IDs in the data ecosystem, which act as legal parents to the current
    entity.
    """
    """An array of none, one or many entity references in the data ecosystem, which identify the
    source of data in the legal sense. Example: the 'parents' will be queried when e.g. the
    subscription of source data services is terminated; access to the derivatives is also
    terminated.
    """
    parents: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ParentList':
        assert isinstance(obj, dict)
        parents = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Parents"))
        return ParentList(parents)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Parents"] = from_union([lambda x: from_list(from_str, x), from_none], self.parents)
        return result


@dataclass
class LineageAssertion:
    id: Optional[str] = None
    relationship_type: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LineageAssertion':
        assert isinstance(obj, dict)
        id = from_union([from_str, from_none], obj.get("ID"))
        relationship_type = from_union([from_str, from_none], obj.get("RelationshipType"))
        return LineageAssertion(id, relationship_type)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ID"] = from_union([from_str, from_none], self.id)
        result["RelationshipType"] = from_union([from_str, from_none], self.relationship_type)
        return result


@dataclass
class AbstractCoordinates:
    """A geographic position on the surface of the earth."""
    """x is Easting or Longitude."""
    x: Optional[float] = None
    """y is Northing or Latitude."""
    y: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractCoordinates':
        assert isinstance(obj, dict)
        x = from_union([from_float, from_none], obj.get("x"))
        y = from_union([from_float, from_none], obj.get("y"))
        return AbstractCoordinates(x, y)

    def to_dict(self) -> dict:
        result: dict = {}
        result["x"] = from_union([to_float, from_none], self.x)
        result["y"] = from_union([to_float, from_none], self.y)
        return result


@dataclass
class AbstractSpatialLocation:
    """A polygon boundary that reflects the locale of the content of the work product (location
    of the subject matter).
    
    A geographic object which can be described by a set of points.
    
    A centroid point that reflects the locale of the content of the work product (location of
    the subject matter).
    """
    """The date of the Quality Check."""
    coordinate_quality_check_date_time: Optional[datetime] = None
    """The user who performed the Quality Check."""
    coordinate_quality_check_performed_by: Optional[str] = None
    """Freetext remark on Quality Check."""
    coordinate_quality_check_remark: Optional[str] = None
    """Projected or geographic coordinates."""
    coordinates: Optional[List[AbstractCoordinates]] = None
    """The elevation of the measured coordinates above the datum expressed by the VerticalCRS."""
    elevation: Optional[float] = None
    """The height of the measured coordinates above the ground."""
    height_above_ground_level: Optional[float] = None
    """Unit of Measure for the height above ground level."""
    height_above_ground_level_uomid: Optional[str] = None
    """Horizontal CRS."""
    horizontal_crsid: Optional[str] = None
    """A qualitative description of the quality of a spatial location, e.g. unverifiable, not
    verified, basic validation.
    """
    qualitative_spatial_accuracy_type_id: Optional[str] = None
    """An approximate quantitative assessment of the quality of a location (accurate to > 500 m
    (i.e. not very accurate)), to < 1 m, etc.
    """
    quantitative_accuracy_band_id: Optional[str] = None
    """Indicates the expected look of the SPATIAL_PARAMETER_TYPE, e.g. a point, a line, a
    polyline (e.g. coastline, made up of vertexes), an area, a volume. E.g. a Well Surface is
    a point (an identifiable feature in its own right), a coastline could be a polyline.
    """
    spatial_geometry_type_id: Optional[str] = None
    """Date when coordinates were measured or retrieved."""
    spatial_location_coordinates_date: Optional[datetime] = None
    """A type of spatial representation of an object, often general (e.g. an Outline, which
    could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore
    Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that
    may be used by Countries).
    """
    spatial_parameter_type_id: Optional[str] = None
    """Vertical CRS."""
    vertical_crsid: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractSpatialLocation':
        assert isinstance(obj, dict)
        coordinate_quality_check_date_time = from_union([from_datetime, from_none], obj.get("CoordinateQualityCheckDateTime"))
        coordinate_quality_check_performed_by = from_union([from_str, from_none], obj.get("CoordinateQualityCheckPerformedBy"))
        coordinate_quality_check_remark = from_union([from_str, from_none], obj.get("CoordinateQualityCheckRemark"))
        coordinates = from_union([lambda x: from_list(AbstractCoordinates.from_dict, x), from_none], obj.get("Coordinates"))
        elevation = from_union([from_float, from_none], obj.get("Elevation"))
        height_above_ground_level = from_union([from_float, from_none], obj.get("HeightAboveGroundLevel"))
        height_above_ground_level_uomid = from_union([from_str, from_none], obj.get("HeightAboveGroundLevelUOMID"))
        horizontal_crsid = from_union([from_str, from_none], obj.get("HorizontalCRSID"))
        qualitative_spatial_accuracy_type_id = from_union([from_str, from_none], obj.get("QualitativeSpatialAccuracyTypeID"))
        quantitative_accuracy_band_id = from_union([from_str, from_none], obj.get("QuantitativeAccuracyBandID"))
        spatial_geometry_type_id = from_union([from_str, from_none], obj.get("SpatialGeometryTypeID"))
        spatial_location_coordinates_date = from_union([from_datetime, from_none], obj.get("SpatialLocationCoordinatesDate"))
        spatial_parameter_type_id = from_union([from_str, from_none], obj.get("SpatialParameterTypeID"))
        vertical_crsid = from_union([from_str, from_none], obj.get("VerticalCRSID"))
        return AbstractSpatialLocation(coordinate_quality_check_date_time, coordinate_quality_check_performed_by, coordinate_quality_check_remark, coordinates, elevation, height_above_ground_level, height_above_ground_level_uomid, horizontal_crsid, qualitative_spatial_accuracy_type_id, quantitative_accuracy_band_id, spatial_geometry_type_id, spatial_location_coordinates_date, spatial_parameter_type_id, vertical_crsid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["CoordinateQualityCheckDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.coordinate_quality_check_date_time)
        result["CoordinateQualityCheckPerformedBy"] = from_union([from_str, from_none], self.coordinate_quality_check_performed_by)
        result["CoordinateQualityCheckRemark"] = from_union([from_str, from_none], self.coordinate_quality_check_remark)
        result["Coordinates"] = from_union([lambda x: from_list(lambda x: to_class(AbstractCoordinates, x), x), from_none], self.coordinates)
        result["Elevation"] = from_union([to_float, from_none], self.elevation)
        result["HeightAboveGroundLevel"] = from_union([to_float, from_none], self.height_above_ground_level)
        result["HeightAboveGroundLevelUOMID"] = from_union([from_str, from_none], self.height_above_ground_level_uomid)
        result["HorizontalCRSID"] = from_union([from_str, from_none], self.horizontal_crsid)
        result["QualitativeSpatialAccuracyTypeID"] = from_union([from_str, from_none], self.qualitative_spatial_accuracy_type_id)
        result["QuantitativeAccuracyBandID"] = from_union([from_str, from_none], self.quantitative_accuracy_band_id)
        result["SpatialGeometryTypeID"] = from_union([from_str, from_none], self.spatial_geometry_type_id)
        result["SpatialLocationCoordinatesDate"] = from_union([lambda x: x.isoformat(), from_none], self.spatial_location_coordinates_date)
        result["SpatialParameterTypeID"] = from_union([from_str, from_none], self.spatial_parameter_type_id)
        result["VerticalCRSID"] = from_union([from_str, from_none], self.vertical_crsid)
        return result


@dataclass
class Data:
    """Array of Annotations"""
    annotations: Optional[List[str]] = None
    """Array of Authors' names of the work product.  Could be a person or company entity."""
    author_i_ds: Optional[List[str]] = None
    """Array of business processes/workflows that the work product has been through (ex. well
    planning, exploration).
    """
    business_activities: Optional[List[str]] = None
    components: Optional[List[str]] = None
    """Date that a resource (work  product here) is formed outside of OSDU before loading (e.g.
    publication date, work product delivery package assembly date).
    """
    creation_date_time: Optional[datetime] = None
    """Description of the purpose of the work product."""
    description: Optional[str] = None
    """A flag that indicates if the work product is searchable, which means covered in the
    search index.
    """
    is_discoverable: Optional[bool] = None
    """A flag that indicates if the work product is undergoing an extended load.  It reflects
    the fact that the work product is in an early stage and may be updated before
    finalization.
    """
    is_extended_load: Optional[bool] = None
    """Defines relationships with other objects upon which the work product depends."""
    lineage_assertions: Optional[List[LineageAssertion]] = None
    """Name of the instance of Work Product - could be a shipment number."""
    name: Optional[str] = None
    """A polygon boundary that reflects the locale of the content of the work product (location
    of the subject matter).
    """
    spatial_area: Optional[AbstractSpatialLocation] = None
    """A centroid point that reflects the locale of the content of the work product (location of
    the subject matter).
    """
    spatial_point: Optional[AbstractSpatialLocation] = None
    """Name of the person that first submitted the work product package to OSDU."""
    submitter_name: Optional[str] = None
    """Array of key words to identify the work product, especially to help in search."""
    tags: Optional[List[str]] = None
    extension_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Data':
        assert isinstance(obj, dict)
        annotations = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Annotations"))
        author_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("AuthorIDs"))
        business_activities = from_union([lambda x: from_list(from_str, x), from_none], obj.get("BusinessActivities"))
        components = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Components"))
        creation_date_time = from_union([from_datetime, from_none], obj.get("CreationDateTime"))
        description = from_union([from_str, from_none], obj.get("Description"))
        is_discoverable = from_union([from_bool, from_none], obj.get("IsDiscoverable"))
        is_extended_load = from_union([from_bool, from_none], obj.get("IsExtendedLoad"))
        lineage_assertions = from_union([lambda x: from_list(LineageAssertion.from_dict, x), from_none], obj.get("LineageAssertions"))
        name = from_union([from_str, from_none], obj.get("Name"))
        spatial_area = from_union([AbstractSpatialLocation.from_dict, from_none], obj.get("SpatialArea"))
        spatial_point = from_union([AbstractSpatialLocation.from_dict, from_none], obj.get("SpatialPoint"))
        submitter_name = from_union([from_str, from_none], obj.get("SubmitterName"))
        tags = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Tags"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        return Data(annotations, author_i_ds, business_activities, components, creation_date_time, description, is_discoverable, is_extended_load, lineage_assertions, name, spatial_area, spatial_point, submitter_name, tags, extension_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Annotations"] = from_union([lambda x: from_list(from_str, x), from_none], self.annotations)
        result["AuthorIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.author_i_ds)
        result["BusinessActivities"] = from_union([lambda x: from_list(from_str, x), from_none], self.business_activities)
        result["Components"] = from_union([lambda x: from_list(from_str, x), from_none], self.components)
        result["CreationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.creation_date_time)
        result["Description"] = from_union([from_str, from_none], self.description)
        result["IsDiscoverable"] = from_union([from_bool, from_none], self.is_discoverable)
        result["IsExtendedLoad"] = from_union([from_bool, from_none], self.is_extended_load)
        result["LineageAssertions"] = from_union([lambda x: from_list(lambda x: to_class(LineageAssertion, x), x), from_none], self.lineage_assertions)
        result["Name"] = from_union([from_str, from_none], self.name)
        result["SpatialArea"] = from_union([lambda x: to_class(AbstractSpatialLocation, x), from_none], self.spatial_area)
        result["SpatialPoint"] = from_union([lambda x: to_class(AbstractSpatialLocation, x), from_none], self.spatial_point)
        result["SubmitterName"] = from_union([from_str, from_none], self.submitter_name)
        result["Tags"] = from_union([lambda x: from_list(from_str, x), from_none], self.tags)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        return result


@dataclass
class LegalMetaData:
    """The entity's legal tags and compliance status.
    
    Legal meta data like legal tags, relevant other countries, legal status.
    """
    """The list of legal tags."""
    legal_tags: List[str]
    """The list of other relevant data countries."""
    other_relevant_data_countries: List[str]
    """The legal status."""
    status: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LegalMetaData':
        assert isinstance(obj, dict)
        legal_tags = from_list(from_str, obj.get("LegalTags"))
        other_relevant_data_countries = from_list(from_str, obj.get("OtherRelevantDataCountries"))
        status = from_union([from_str, from_none], obj.get("Status"))
        return LegalMetaData(legal_tags, other_relevant_data_countries, status)

    def to_dict(self) -> dict:
        result: dict = {}
        result["LegalTags"] = from_list(from_str, self.legal_tags)
        result["OtherRelevantDataCountries"] = from_list(from_str, self.other_relevant_data_countries)
        result["Status"] = from_union([from_str, from_none], self.status)
        return result


class ReferenceKind(Enum):
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    AZIMUTH_REFERENCE = "AzimuthReference"
    CRS = "CRS"
    DATE_TIME = "DateTime"
    MEASUREMENT = "Measurement"
    UNIT = "Unit"


@dataclass
class FrameOfReferenceMetaDataItem:
    """A meta data item, which allows the association of named properties or property values to
    a Unit/Measurement/CRS/Azimuth/Time context.
    """
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    kind: ReferenceKind
    """The persistable reference string uniquely identifying the CRS or Unit."""
    persistable_reference: str
    """The name of the CRS or the symbol/name of the unit."""
    name: Optional[str] = None
    """The list of property names, to which this meta data item provides Unit/CRS context to.
    Data structures, which come in a single frame of reference, can register the property
    name, others require a full path like "data.structureA.propertyB" to define a unique
    context.
    """
    property_names: Optional[List[str]] = None
    """The list of property values, to which this meta data item provides Unit/CRS context to.
    Typically a unit symbol is a value to a data structure; this symbol is then registered in
    this propertyValues array and the persistableReference provides the absolute reference.
    """
    property_values: Optional[List[str]] = None
    """The uncertainty of the values measured given the unit or CRS unit."""
    uncertainty: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FrameOfReferenceMetaDataItem':
        assert isinstance(obj, dict)
        kind = ReferenceKind(obj.get("Kind"))
        persistable_reference = from_str(obj.get("PersistableReference"))
        name = from_union([from_str, from_none], obj.get("Name"))
        property_names = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyNames"))
        property_values = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyValues"))
        uncertainty = from_union([from_float, from_none], obj.get("Uncertainty"))
        return FrameOfReferenceMetaDataItem(kind, persistable_reference, name, property_names, property_values, uncertainty)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Kind"] = to_enum(ReferenceKind, self.kind)
        result["PersistableReference"] = from_str(self.persistable_reference)
        result["Name"] = from_union([from_str, from_none], self.name)
        result["PropertyNames"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_names)
        result["PropertyValues"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_values)
        result["Uncertainty"] = from_union([to_float, from_none], self.uncertainty)
        return result


@dataclass
class WorkProduct:
    """A collection of work product components such as might be produced by a business activity
    and which is delivered to the data platform for loading.
    """
    """The OSDU GroupType assigned to this resource object."""
    group_type: Any
    """The SRN which identifies this OSDU resource object without version."""
    id: str
    """The schema identification for the OSDU resource object following the pattern
    <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning
    scheme follows the semantic versioning, https://semver.org/.
    """
    kind: str
    """The entity's legal tags and compliance status."""
    legal: LegalMetaData
    """Indicates what kind of ownership Company has over data."""
    license_state: Any
    """Timestamp of the time at which Version 1 of this OSDU resource object was originated."""
    resource_object_creation_date_time: datetime
    """Timestamp of the time when the current version of this resource entered the OSDU."""
    resource_version_creation_date_time: datetime
    """The version number of this OSDU resource; set by the framework."""
    version: float
    """The access control tags associated with this entity."""
    acl: Optional[AccessControlList] = None
    """The links to data, which constitute the inputs."""
    ancestry: Optional[ParentList] = None
    data: Optional[Data] = None
    """Where does this data resource sit in the cradle-to-grave span of its existence?"""
    existence_kind: Optional[str] = None
    """The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions."""
    persistable_references: Optional[List[FrameOfReferenceMetaDataItem]] = None
    """Describes the current Curation status."""
    resource_curation_status: Optional[str] = None
    """The name of the home [cloud environment] region for this OSDU resource object."""
    resource_home_region_id: Optional[str] = None
    """The name of the host [cloud environment] region(s) for this OSDU resource object."""
    resource_host_region_i_ds: Optional[List[str]] = None
    """Describes the current Resource Lifecycle status."""
    resource_lifecycle_status: Optional[str] = None
    """Classifies the security level of the resource."""
    resource_security_classification: Optional[str] = None
    """Where did the data resource originate? This could be many kinds of entities, such as
    company, agency, team or individual.
    """
    source: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProduct':
        assert isinstance(obj, dict)
        group_type = obj.get("GroupType")
        id = from_str(obj.get("ID"))
        kind = from_str(obj.get("Kind"))
        legal = LegalMetaData.from_dict(obj.get("Legal"))
        license_state = obj.get("LicenseState")
        resource_object_creation_date_time = from_datetime(obj.get("ResourceObjectCreationDateTime"))
        resource_version_creation_date_time = from_datetime(obj.get("ResourceVersionCreationDateTime"))
        version = from_float(obj.get("Version"))
        acl = from_union([AccessControlList.from_dict, from_none], obj.get("ACL"))
        ancestry = from_union([ParentList.from_dict, from_none], obj.get("Ancestry"))
        data = from_union([Data.from_dict, from_none], obj.get("Data"))
        existence_kind = from_union([from_str, from_none], obj.get("ExistenceKind"))
        persistable_references = from_union([lambda x: from_list(FrameOfReferenceMetaDataItem.from_dict, x), from_none], obj.get("PersistableReferences"))
        resource_curation_status = from_union([from_str, from_none], obj.get("ResourceCurationStatus"))
        resource_home_region_id = from_union([from_str, from_none], obj.get("ResourceHomeRegionID"))
        resource_host_region_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("ResourceHostRegionIDs"))
        resource_lifecycle_status = from_union([from_str, from_none], obj.get("ResourceLifecycleStatus"))
        resource_security_classification = from_union([from_str, from_none], obj.get("ResourceSecurityClassification"))
        source = from_union([from_str, from_none], obj.get("Source"))
        return WorkProduct(group_type, id, kind, legal, license_state, resource_object_creation_date_time, resource_version_creation_date_time, version, acl, ancestry, data, existence_kind, persistable_references, resource_curation_status, resource_home_region_id, resource_host_region_i_ds, resource_lifecycle_status, resource_security_classification, source)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GroupType"] = self.group_type
        result["ID"] = from_str(self.id)
        result["Kind"] = from_str(self.kind)
        result["Legal"] = to_class(LegalMetaData, self.legal)
        result["LicenseState"] = self.license_state
        result["ResourceObjectCreationDateTime"] = self.resource_object_creation_date_time.isoformat()
        result["ResourceVersionCreationDateTime"] = self.resource_version_creation_date_time.isoformat()
        result["Version"] = to_float(self.version)
        result["ACL"] = from_union([lambda x: to_class(AccessControlList, x), from_none], self.acl)
        result["Ancestry"] = from_union([lambda x: to_class(ParentList, x), from_none], self.ancestry)
        result["Data"] = from_union([lambda x: to_class(Data, x), from_none], self.data)
        result["ExistenceKind"] = from_union([from_str, from_none], self.existence_kind)
        result["PersistableReferences"] = from_union([lambda x: from_list(lambda x: to_class(FrameOfReferenceMetaDataItem, x), x), from_none], self.persistable_references)
        result["ResourceCurationStatus"] = from_union([from_str, from_none], self.resource_curation_status)
        result["ResourceHomeRegionID"] = from_union([from_str, from_none], self.resource_home_region_id)
        result["ResourceHostRegionIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.resource_host_region_i_ds)
        result["ResourceLifecycleStatus"] = from_union([from_str, from_none], self.resource_lifecycle_status)
        result["ResourceSecurityClassification"] = from_union([from_str, from_none], self.resource_security_classification)
        result["Source"] = from_union([from_str, from_none], self.source)
        return result


def work_product_from_dict(s: Any) -> WorkProduct:
    return WorkProduct.from_dict(s)


def work_product_to_dict(x: WorkProduct) -> Any:
    return to_class(WorkProduct, x)
