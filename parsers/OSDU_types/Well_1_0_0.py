# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = well_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import List, Any, Optional, Dict, TypeVar, Callable, Type, cast
from datetime import datetime
from enum import Enum
import dateutil.parser


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return { k: f(v) for (k, v) in x.items() }


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


@dataclass
class AccessControlList:
    """The access control tags associated with this entity."""
    """The list of owners of this data record."""
    owners: List[str]
    """The list of viewers to which this data record is accessible/visible/discoverable."""
    viewers: List[str]

    @staticmethod
    def from_dict(obj: Any) -> 'AccessControlList':
        assert isinstance(obj, dict)
        owners = from_list(from_str, obj.get("Owners"))
        viewers = from_list(from_str, obj.get("Viewers"))
        return AccessControlList(owners, viewers)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Owners"] = from_list(from_str, self.owners)
        result["Viewers"] = from_list(from_str, self.viewers)
        return result


@dataclass
class ParentList:
    """The links to data, which constitute the inputs.
    
    A list of entity IDs in the data ecosystem, which act as legal parents to the current
    entity.
    """
    """An array of none, one or many entity references in the data ecosystem, which identify the
    source of data in the legal sense. Example: the 'parents' will be queried when e.g. the
    subscription of source data services is terminated; access to the derivatives is also
    terminated.
    """
    parents: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ParentList':
        assert isinstance(obj, dict)
        parents = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Parents"))
        return ParentList(parents)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Parents"] = from_union([lambda x: from_list(from_str, x), from_none], self.parents)
        return result


@dataclass
class AbstractFacilityEvent:
    """A significant occurrence in the life of a facility, which often changes its state, or the
    state of one of its components.
    """
    """The date and time at which the event becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The facility event type is a picklist. Examples: Propose, Completion, Entry Date etc."""
    facility_event_type_id: Optional[str] = None
    """The date and time at which the event is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractFacilityEvent':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        facility_event_type_id = from_union([from_str, from_none], obj.get("FacilityEventTypeID"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return AbstractFacilityEvent(effective_date_time, facility_event_type_id, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["FacilityEventTypeID"] = from_union([from_str, from_none], self.facility_event_type_id)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class AbstractAliasNames:
    """A list of alternative names for an object.  The preferred name is in a separate, scalar
    property.  It may or may not be repeated in the alias list, though a best practice is to
    include it if the list is present, but to omit the list if there are no other names.
    Note that the abstract entity is an array so the $ref to it is a simple property
    reference.
    """
    """Alternative Name value of defined name type for an object."""
    alias_name: Optional[str] = None
    """A classification of alias names such as by role played or type of source, such as
    regulatory name, regulatory code, company code, international standard name, etc.
    """
    alias_name_type_id: Optional[str] = None
    """Organisation that provided the name (the source)."""
    definition_organisation_id: Optional[str] = None
    """The date and time when an alias name becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The data and time when an alias name is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractAliasNames':
        assert isinstance(obj, dict)
        alias_name = from_union([from_str, from_none], obj.get("AliasName"))
        alias_name_type_id = from_union([from_str, from_none], obj.get("AliasNameTypeID"))
        definition_organisation_id = from_union([from_str, from_none], obj.get("DefinitionOrganisationID"))
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return AbstractAliasNames(alias_name, alias_name_type_id, definition_organisation_id, effective_date_time, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["AliasName"] = from_union([from_str, from_none], self.alias_name)
        result["AliasNameTypeID"] = from_union([from_str, from_none], self.alias_name_type_id)
        result["DefinitionOrganisationID"] = from_union([from_str, from_none], self.definition_organisation_id)
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class AbstractFacilityOperator:
    """The organisation that was responsible for a facility at some point in time."""
    """The date and time at which the facility operator becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The company that currently operates, or previously operated the facility"""
    facility_operator_organisation_id: Optional[str] = None
    """The date and time at which the facility operator is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractFacilityOperator':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        facility_operator_organisation_id = from_union([from_str, from_none], obj.get("FacilityOperatorOrganisationID"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return AbstractFacilityOperator(effective_date_time, facility_operator_organisation_id, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["FacilityOperatorOrganisationID"] = from_union([from_str, from_none], self.facility_operator_organisation_id)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class AbstractFacilitySpecification:
    """A property, characteristic, or attribute about a facility that is not described
    explicitly elsewhere.
    """
    """The date and time at which the facility specification instance becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The actual date and time value of the parameter."""
    facility_specification_date_time: Optional[datetime] = None
    """The actual indicator value of the parameter."""
    facility_specification_indicator: Optional[bool] = None
    """The value for the specified parameter type."""
    facility_specification_quantity: Optional[float] = None
    """The actual text value of the parameter."""
    facility_specification_text: Optional[str] = None
    """Parameter type of property or characteristic."""
    parameter_type_id: Optional[str] = None
    """The date and time at which the facility specification instance is no longer in effect."""
    termination_date_time: Optional[datetime] = None
    """The unit for the quantity parameter, like metre (m in SI units system) for quantity
    Length.
    """
    unit_of_measure_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractFacilitySpecification':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        facility_specification_date_time = from_union([from_datetime, from_none], obj.get("FacilitySpecificationDateTime"))
        facility_specification_indicator = from_union([from_bool, from_none], obj.get("FacilitySpecificationIndicator"))
        facility_specification_quantity = from_union([from_float, from_none], obj.get("FacilitySpecificationQuantity"))
        facility_specification_text = from_union([from_str, from_none], obj.get("FacilitySpecificationText"))
        parameter_type_id = from_union([from_str, from_none], obj.get("ParameterTypeID"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        unit_of_measure_id = from_union([from_str, from_none], obj.get("UnitOfMeasureID"))
        return AbstractFacilitySpecification(effective_date_time, facility_specification_date_time, facility_specification_indicator, facility_specification_quantity, facility_specification_text, parameter_type_id, termination_date_time, unit_of_measure_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["FacilitySpecificationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.facility_specification_date_time)
        result["FacilitySpecificationIndicator"] = from_union([from_bool, from_none], self.facility_specification_indicator)
        result["FacilitySpecificationQuantity"] = from_union([to_float, from_none], self.facility_specification_quantity)
        result["FacilitySpecificationText"] = from_union([from_str, from_none], self.facility_specification_text)
        result["ParameterTypeID"] = from_union([from_str, from_none], self.parameter_type_id)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        result["UnitOfMeasureID"] = from_union([from_str, from_none], self.unit_of_measure_id)
        return result


@dataclass
class AbstractFacilityState:
    """The life cycle status of a facility at some point in time."""
    """The date and time at which the facility state becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The facility life cycle state from planning to abandonment."""
    facility_state_type_id: Optional[str] = None
    """The date and time at which the facility state is no longer in effect."""
    termination_date_time: Optional[datetime] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractFacilityState':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        facility_state_type_id = from_union([from_str, from_none], obj.get("FacilityStateTypeID"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        return AbstractFacilityState(effective_date_time, facility_state_type_id, termination_date_time)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["FacilityStateTypeID"] = from_union([from_str, from_none], self.facility_state_type_id)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        return result


@dataclass
class AbstractGeoContext:
    """A geographic context to an entity. It can be either a reference to a GeoPoliticalEntity,
    Basin, Field, Play or Prospect.
    
    A single, typed geo-political entity reference, which is 'abstracted' to
    AbstractGeoContext and then aggregated by GeoContexts properties.
    
    A single, typed basin entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed field entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed Play entity reference, which is 'abstracted' to AbstractGeoContext and
    then aggregated by GeoContexts properties.
    
    A single, typed Prospect entity reference, which is 'abstracted' to AbstractGeoContext
    and then aggregated by GeoContexts properties.
    """
    """The GeoPoliticalEntityType reference of the GeoPoliticalEntity (via GeoPoliticalEntityID)
    for application convenience.
    
    The BasinType reference of the Basin (via BasinID) for application convenience.
    
    The fixed type 'Field' for this AbstractGeoFieldContext.
    
    The PlayType reference of the Play (via PlayID) for application convenience.
    
    The ProspectType reference of the Prospect (via ProspectID) for application convenience.
    """
    geo_type_id: Any
    """Reference to GeoPoliticalEntity."""
    geo_political_entity_id: Optional[str] = None
    """Reference to Basin."""
    basin_id: Optional[str] = None
    """Reference to Field."""
    field_id: Optional[str] = None
    """Reference to the play."""
    play_id: Optional[str] = None
    """Reference to the prospect."""
    prospect_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractGeoContext':
        assert isinstance(obj, dict)
        geo_type_id = obj.get("GeoTypeID")
        geo_political_entity_id = from_union([from_str, from_none], obj.get("GeoPoliticalEntityID"))
        basin_id = from_union([from_str, from_none], obj.get("BasinID"))
        field_id = from_union([from_str, from_none], obj.get("FieldID"))
        play_id = from_union([from_str, from_none], obj.get("PlayID"))
        prospect_id = from_union([from_str, from_none], obj.get("ProspectID"))
        return AbstractGeoContext(geo_type_id, geo_political_entity_id, basin_id, field_id, play_id, prospect_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GeoTypeID"] = self.geo_type_id
        result["GeoPoliticalEntityID"] = from_union([from_str, from_none], self.geo_political_entity_id)
        result["BasinID"] = from_union([from_str, from_none], self.basin_id)
        result["FieldID"] = from_union([from_str, from_none], self.field_id)
        result["PlayID"] = from_union([from_str, from_none], self.play_id)
        result["ProspectID"] = from_union([from_str, from_none], self.prospect_id)
        return result


@dataclass
class AbstractCoordinates:
    """A geographic position on the surface of the earth."""
    """x is Easting or Longitude."""
    x: Optional[float] = None
    """y is Northing or Latitude."""
    y: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractCoordinates':
        assert isinstance(obj, dict)
        x = from_union([from_float, from_none], obj.get("x"))
        y = from_union([from_float, from_none], obj.get("y"))
        return AbstractCoordinates(x, y)

    def to_dict(self) -> dict:
        result: dict = {}
        result["x"] = from_union([to_float, from_none], self.x)
        result["y"] = from_union([to_float, from_none], self.y)
        return result


@dataclass
class AbstractSpatialLocation:
    """A geographic object which can be described by a set of points."""
    """The date of the Quality Check."""
    coordinate_quality_check_date_time: Optional[datetime] = None
    """The user who performed the Quality Check."""
    coordinate_quality_check_performed_by: Optional[str] = None
    """Freetext remark on Quality Check."""
    coordinate_quality_check_remark: Optional[str] = None
    """Projected or geographic coordinates."""
    coordinates: Optional[List[AbstractCoordinates]] = None
    """The elevation of the measured coordinates above the datum expressed by the VerticalCRS."""
    elevation: Optional[float] = None
    """The height of the measured coordinates above the ground."""
    height_above_ground_level: Optional[float] = None
    """Unit of Measure for the height above ground level."""
    height_above_ground_level_uomid: Optional[str] = None
    """Horizontal CRS."""
    horizontal_crsid: Optional[str] = None
    """A qualitative description of the quality of a spatial location, e.g. unverifiable, not
    verified, basic validation.
    """
    qualitative_spatial_accuracy_type_id: Optional[str] = None
    """An approximate quantitative assessment of the quality of a location (accurate to > 500 m
    (i.e. not very accurate)), to < 1 m, etc.
    """
    quantitative_accuracy_band_id: Optional[str] = None
    """Indicates the expected look of the SPATIAL_PARAMETER_TYPE, e.g. a point, a line, a
    polyline (e.g. coastline, made up of vertexes), an area, a volume. E.g. a Well Surface is
    a point (an identifiable feature in its own right), a coastline could be a polyline.
    """
    spatial_geometry_type_id: Optional[str] = None
    """Date when coordinates were measured or retrieved."""
    spatial_location_coordinates_date: Optional[datetime] = None
    """A type of spatial representation of an object, often general (e.g. an Outline, which
    could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore
    Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that
    may be used by Countries).
    """
    spatial_parameter_type_id: Optional[str] = None
    """Vertical CRS."""
    vertical_crsid: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractSpatialLocation':
        assert isinstance(obj, dict)
        coordinate_quality_check_date_time = from_union([from_datetime, from_none], obj.get("CoordinateQualityCheckDateTime"))
        coordinate_quality_check_performed_by = from_union([from_str, from_none], obj.get("CoordinateQualityCheckPerformedBy"))
        coordinate_quality_check_remark = from_union([from_str, from_none], obj.get("CoordinateQualityCheckRemark"))
        coordinates = from_union([lambda x: from_list(AbstractCoordinates.from_dict, x), from_none], obj.get("Coordinates"))
        elevation = from_union([from_float, from_none], obj.get("Elevation"))
        height_above_ground_level = from_union([from_float, from_none], obj.get("HeightAboveGroundLevel"))
        height_above_ground_level_uomid = from_union([from_str, from_none], obj.get("HeightAboveGroundLevelUOMID"))
        horizontal_crsid = from_union([from_str, from_none], obj.get("HorizontalCRSID"))
        qualitative_spatial_accuracy_type_id = from_union([from_str, from_none], obj.get("QualitativeSpatialAccuracyTypeID"))
        quantitative_accuracy_band_id = from_union([from_str, from_none], obj.get("QuantitativeAccuracyBandID"))
        spatial_geometry_type_id = from_union([from_str, from_none], obj.get("SpatialGeometryTypeID"))
        spatial_location_coordinates_date = from_union([from_datetime, from_none], obj.get("SpatialLocationCoordinatesDate"))
        spatial_parameter_type_id = from_union([from_str, from_none], obj.get("SpatialParameterTypeID"))
        vertical_crsid = from_union([from_str, from_none], obj.get("VerticalCRSID"))
        return AbstractSpatialLocation(coordinate_quality_check_date_time, coordinate_quality_check_performed_by, coordinate_quality_check_remark, coordinates, elevation, height_above_ground_level, height_above_ground_level_uomid, horizontal_crsid, qualitative_spatial_accuracy_type_id, quantitative_accuracy_band_id, spatial_geometry_type_id, spatial_location_coordinates_date, spatial_parameter_type_id, vertical_crsid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["CoordinateQualityCheckDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.coordinate_quality_check_date_time)
        result["CoordinateQualityCheckPerformedBy"] = from_union([from_str, from_none], self.coordinate_quality_check_performed_by)
        result["CoordinateQualityCheckRemark"] = from_union([from_str, from_none], self.coordinate_quality_check_remark)
        result["Coordinates"] = from_union([lambda x: from_list(lambda x: to_class(AbstractCoordinates, x), x), from_none], self.coordinates)
        result["Elevation"] = from_union([to_float, from_none], self.elevation)
        result["HeightAboveGroundLevel"] = from_union([to_float, from_none], self.height_above_ground_level)
        result["HeightAboveGroundLevelUOMID"] = from_union([from_str, from_none], self.height_above_ground_level_uomid)
        result["HorizontalCRSID"] = from_union([from_str, from_none], self.horizontal_crsid)
        result["QualitativeSpatialAccuracyTypeID"] = from_union([from_str, from_none], self.qualitative_spatial_accuracy_type_id)
        result["QuantitativeAccuracyBandID"] = from_union([from_str, from_none], self.quantitative_accuracy_band_id)
        result["SpatialGeometryTypeID"] = from_union([from_str, from_none], self.spatial_geometry_type_id)
        result["SpatialLocationCoordinatesDate"] = from_union([lambda x: x.isoformat(), from_none], self.spatial_location_coordinates_date)
        result["SpatialParameterTypeID"] = from_union([from_str, from_none], self.spatial_parameter_type_id)
        result["VerticalCRSID"] = from_union([from_str, from_none], self.vertical_crsid)
        return result


@dataclass
class AbstractFacilityVerticalMeasurement:
    """A location along a wellbore, _usually_ associated with some aspect of the drilling of the
    wellbore, but not with any intersecting _subsurface_ natural surfaces.
    """
    """The date and time at which a vertical measurement instance becomes effective."""
    effective_date_time: Optional[datetime] = None
    """The date and time at which a vertical measurement instance is no longer in effect."""
    termination_date_time: Optional[datetime] = None
    """Vertical CRS. It is expected that a Vertical CRS or a Vertical Reference is provided, but
    not both.
    """
    vertical_crsid: Optional[str] = None
    """The value of the elevation or depth. Depth is positive downwards from a vertical
    reference or geodetic datum along a path, which can be vertical; elevation is positive
    upwards from a geodetic datum along a vertical path. Either can be negative.
    """
    vertical_measurement: Optional[float] = None
    """Text which describes a vertical measurement in detail."""
    vertical_measurement_description: Optional[str] = None
    """The ID for a distinct vertical measurement within the Facility array so that it may be
    referenced by other vertical measurements if necessary.
    """
    vertical_measurement_id: Optional[str] = None
    """Specifies Measured Depth, True Vertical Depth, or Elevation."""
    vertical_measurement_path_id: Optional[str] = None
    """Specifies Driller vs Logger."""
    vertical_measurement_source_id: Optional[str] = None
    """Specifies the type of vertical measurement (TD, Plugback, Kickoff, Drill Floor, Rotary
    Table...).
    """
    vertical_measurement_type_id: Optional[str] = None
    """The unit of measure for the vertical measurement. If a unit of measure and a vertical CRS
    are provided, the unit of measure provided is taken over the unit of measure from the CRS.
    """
    vertical_measurement_unit_of_measure_id: Optional[str] = None
    """The reference point from which the vertical measurement is made. Must resolve ultimately
    to a vertical CRS. It is expected that a Vertical CRS or a Vertical Reference is
    provided, but not both.
    """
    vertical_reference_id: Optional[str] = None
    """Specifies what directional survey or wellpath was used to calculate the TVD."""
    wellbore_tvd_trajectory_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AbstractFacilityVerticalMeasurement':
        assert isinstance(obj, dict)
        effective_date_time = from_union([from_datetime, from_none], obj.get("EffectiveDateTime"))
        termination_date_time = from_union([from_datetime, from_none], obj.get("TerminationDateTime"))
        vertical_crsid = from_union([from_str, from_none], obj.get("VerticalCRSID"))
        vertical_measurement = from_union([from_float, from_none], obj.get("VerticalMeasurement"))
        vertical_measurement_description = from_union([from_str, from_none], obj.get("VerticalMeasurementDescription"))
        vertical_measurement_id = from_union([from_str, from_none], obj.get("VerticalMeasurementID"))
        vertical_measurement_path_id = from_union([from_str, from_none], obj.get("VerticalMeasurementPathID"))
        vertical_measurement_source_id = from_union([from_str, from_none], obj.get("VerticalMeasurementSourceID"))
        vertical_measurement_type_id = from_union([from_str, from_none], obj.get("VerticalMeasurementTypeID"))
        vertical_measurement_unit_of_measure_id = from_union([from_str, from_none], obj.get("VerticalMeasurementUnitOfMeasureID"))
        vertical_reference_id = from_union([from_str, from_none], obj.get("VerticalReferenceID"))
        wellbore_tvd_trajectory_id = from_union([from_str, from_none], obj.get("WellboreTVDTrajectoryID"))
        return AbstractFacilityVerticalMeasurement(effective_date_time, termination_date_time, vertical_crsid, vertical_measurement, vertical_measurement_description, vertical_measurement_id, vertical_measurement_path_id, vertical_measurement_source_id, vertical_measurement_type_id, vertical_measurement_unit_of_measure_id, vertical_reference_id, wellbore_tvd_trajectory_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["EffectiveDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.effective_date_time)
        result["TerminationDateTime"] = from_union([lambda x: x.isoformat(), from_none], self.termination_date_time)
        result["VerticalCRSID"] = from_union([from_str, from_none], self.vertical_crsid)
        result["VerticalMeasurement"] = from_union([to_float, from_none], self.vertical_measurement)
        result["VerticalMeasurementDescription"] = from_union([from_str, from_none], self.vertical_measurement_description)
        result["VerticalMeasurementID"] = from_union([from_str, from_none], self.vertical_measurement_id)
        result["VerticalMeasurementPathID"] = from_union([from_str, from_none], self.vertical_measurement_path_id)
        result["VerticalMeasurementSourceID"] = from_union([from_str, from_none], self.vertical_measurement_source_id)
        result["VerticalMeasurementTypeID"] = from_union([from_str, from_none], self.vertical_measurement_type_id)
        result["VerticalMeasurementUnitOfMeasureID"] = from_union([from_str, from_none], self.vertical_measurement_unit_of_measure_id)
        result["VerticalReferenceID"] = from_union([from_str, from_none], self.vertical_reference_id)
        result["WellboreTVDTrajectoryID"] = from_union([from_str, from_none], self.wellbore_tvd_trajectory_id)
        return result


@dataclass
class Data:
    """The main source of the header information."""
    data_source_organisation_id: Optional[str] = None
    """A list of key facility events."""
    facility_event: Optional[List[AbstractFacilityEvent]] = None
    """A system-specified unique identifier of a Facility."""
    facility_id: Optional[str] = None
    """Name of the Facility."""
    facility_name: Optional[str] = None
    """Alternative names, including historical, by which this facility is/has been known."""
    facility_name_alias: Optional[List[AbstractAliasNames]] = None
    """The history of operator organizations of the facility."""
    facility_operator: Optional[List[AbstractFacilityOperator]] = None
    """facilitySpecification maintains the specification like slot name, wellbore drilling
    permit number, rig name etc.
    """
    facility_specification: Optional[List[AbstractFacilitySpecification]] = None
    """The history of life cycle states the facility has been through."""
    facility_state: Optional[List[AbstractFacilityState]] = None
    """The definition of a kind of capability to perform a business function or a service."""
    facility_type_id: Optional[str] = None
    """List of geographic entities which provide context to the facility.  This may include
    multiple types or multiple values of the same type.
    """
    geo_contexts: Optional[List[AbstractGeoContext]] = None
    """Identifies the Facility's general location as being onshore vs. offshore."""
    operating_environment_id: Optional[str] = None
    """The spatial location information such as coordinates,CRS information."""
    spatial_location: Optional[List[AbstractSpatialLocation]] = None
    """The default vertical coordinate reference system used in the vertical measurements for a
    well or wellbore if absent from input vertical measurements and there is no other
    recourse for obtaining a valid CRS.
    """
    default_vertical_crsid: Optional[str] = None
    """The default datum reference point, or zero depth point, used to determine other points
    vertically in a well.  References an entry in the VerticalMeasurements array.
    """
    default_vertical_measurement_id: Optional[str] = None
    """Pre-defined reasons for interest in the well or information about the well."""
    interest_type_id: Optional[str] = None
    """List of all depths and elevations pertaining to the well, like, water depth, mud line
    elevation, etc.
    """
    vertical_measurements: Optional[List[AbstractFacilityVerticalMeasurement]] = None
    extension_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Data':
        assert isinstance(obj, dict)
        data_source_organisation_id = from_union([from_str, from_none], obj.get("DataSourceOrganisationID"))
        facility_event = from_union([lambda x: from_list(AbstractFacilityEvent.from_dict, x), from_none], obj.get("FacilityEvent"))
        facility_id = from_union([from_str, from_none], obj.get("FacilityID"))
        facility_name = from_union([from_str, from_none], obj.get("FacilityName"))
        facility_name_alias = from_union([lambda x: from_list(AbstractAliasNames.from_dict, x), from_none], obj.get("FacilityNameAlias"))
        facility_operator = from_union([lambda x: from_list(AbstractFacilityOperator.from_dict, x), from_none], obj.get("FacilityOperator"))
        facility_specification = from_union([lambda x: from_list(AbstractFacilitySpecification.from_dict, x), from_none], obj.get("FacilitySpecification"))
        facility_state = from_union([lambda x: from_list(AbstractFacilityState.from_dict, x), from_none], obj.get("FacilityState"))
        facility_type_id = from_union([from_str, from_none], obj.get("FacilityTypeID"))
        geo_contexts = from_union([lambda x: from_list(AbstractGeoContext.from_dict, x), from_none], obj.get("GeoContexts"))
        operating_environment_id = from_union([from_str, from_none], obj.get("OperatingEnvironmentID"))
        spatial_location = from_union([lambda x: from_list(AbstractSpatialLocation.from_dict, x), from_none], obj.get("SpatialLocation"))
        default_vertical_crsid = from_union([from_str, from_none], obj.get("DefaultVerticalCRSID"))
        default_vertical_measurement_id = from_union([from_str, from_none], obj.get("DefaultVerticalMeasurementID"))
        interest_type_id = from_union([from_str, from_none], obj.get("InterestTypeID"))
        vertical_measurements = from_union([lambda x: from_list(AbstractFacilityVerticalMeasurement.from_dict, x), from_none], obj.get("VerticalMeasurements"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        return Data(data_source_organisation_id, facility_event, facility_id, facility_name, facility_name_alias, facility_operator, facility_specification, facility_state, facility_type_id, geo_contexts, operating_environment_id, spatial_location, default_vertical_crsid, default_vertical_measurement_id, interest_type_id, vertical_measurements, extension_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["DataSourceOrganisationID"] = from_union([from_str, from_none], self.data_source_organisation_id)
        result["FacilityEvent"] = from_union([lambda x: from_list(lambda x: to_class(AbstractFacilityEvent, x), x), from_none], self.facility_event)
        result["FacilityID"] = from_union([from_str, from_none], self.facility_id)
        result["FacilityName"] = from_union([from_str, from_none], self.facility_name)
        result["FacilityNameAlias"] = from_union([lambda x: from_list(lambda x: to_class(AbstractAliasNames, x), x), from_none], self.facility_name_alias)
        result["FacilityOperator"] = from_union([lambda x: from_list(lambda x: to_class(AbstractFacilityOperator, x), x), from_none], self.facility_operator)
        result["FacilitySpecification"] = from_union([lambda x: from_list(lambda x: to_class(AbstractFacilitySpecification, x), x), from_none], self.facility_specification)
        result["FacilityState"] = from_union([lambda x: from_list(lambda x: to_class(AbstractFacilityState, x), x), from_none], self.facility_state)
        result["FacilityTypeID"] = from_union([from_str, from_none], self.facility_type_id)
        result["GeoContexts"] = from_union([lambda x: from_list(lambda x: to_class(AbstractGeoContext, x), x), from_none], self.geo_contexts)
        result["OperatingEnvironmentID"] = from_union([from_str, from_none], self.operating_environment_id)
        result["SpatialLocation"] = from_union([lambda x: from_list(lambda x: to_class(AbstractSpatialLocation, x), x), from_none], self.spatial_location)
        result["DefaultVerticalCRSID"] = from_union([from_str, from_none], self.default_vertical_crsid)
        result["DefaultVerticalMeasurementID"] = from_union([from_str, from_none], self.default_vertical_measurement_id)
        result["InterestTypeID"] = from_union([from_str, from_none], self.interest_type_id)
        result["VerticalMeasurements"] = from_union([lambda x: from_list(lambda x: to_class(AbstractFacilityVerticalMeasurement, x), x), from_none], self.vertical_measurements)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        return result


@dataclass
class LegalMetaData:
    """The entity's legal tags and compliance status.
    
    Legal meta data like legal tags, relevant other countries, legal status.
    """
    """The list of legal tags."""
    legal_tags: List[str]
    """The list of other relevant data countries."""
    other_relevant_data_countries: List[str]
    """The legal status."""
    status: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LegalMetaData':
        assert isinstance(obj, dict)
        legal_tags = from_list(from_str, obj.get("LegalTags"))
        other_relevant_data_countries = from_list(from_str, obj.get("OtherRelevantDataCountries"))
        status = from_union([from_str, from_none], obj.get("Status"))
        return LegalMetaData(legal_tags, other_relevant_data_countries, status)

    def to_dict(self) -> dict:
        result: dict = {}
        result["LegalTags"] = from_list(from_str, self.legal_tags)
        result["OtherRelevantDataCountries"] = from_list(from_str, self.other_relevant_data_countries)
        result["Status"] = from_union([from_str, from_none], self.status)
        return result


class ReferenceKind(Enum):
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    AZIMUTH_REFERENCE = "AzimuthReference"
    CRS = "CRS"
    DATE_TIME = "DateTime"
    MEASUREMENT = "Measurement"
    UNIT = "Unit"


@dataclass
class FrameOfReferenceMetaDataItem:
    """A meta data item, which allows the association of named properties or property values to
    a Unit/Measurement/CRS/Azimuth/Time context.
    """
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    kind: ReferenceKind
    """The persistable reference string uniquely identifying the CRS or Unit."""
    persistable_reference: str
    """The name of the CRS or the symbol/name of the unit."""
    name: Optional[str] = None
    """The list of property names, to which this meta data item provides Unit/CRS context to.
    Data structures, which come in a single frame of reference, can register the property
    name, others require a full path like "data.structureA.propertyB" to define a unique
    context.
    """
    property_names: Optional[List[str]] = None
    """The list of property values, to which this meta data item provides Unit/CRS context to.
    Typically a unit symbol is a value to a data structure; this symbol is then registered in
    this propertyValues array and the persistableReference provides the absolute reference.
    """
    property_values: Optional[List[str]] = None
    """The uncertainty of the values measured given the unit or CRS unit."""
    uncertainty: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FrameOfReferenceMetaDataItem':
        assert isinstance(obj, dict)
        kind = ReferenceKind(obj.get("Kind"))
        persistable_reference = from_str(obj.get("PersistableReference"))
        name = from_union([from_str, from_none], obj.get("Name"))
        property_names = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyNames"))
        property_values = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyValues"))
        uncertainty = from_union([from_float, from_none], obj.get("Uncertainty"))
        return FrameOfReferenceMetaDataItem(kind, persistable_reference, name, property_names, property_values, uncertainty)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Kind"] = to_enum(ReferenceKind, self.kind)
        result["PersistableReference"] = from_str(self.persistable_reference)
        result["Name"] = from_union([from_str, from_none], self.name)
        result["PropertyNames"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_names)
        result["PropertyValues"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_values)
        result["Uncertainty"] = from_union([to_float, from_none], self.uncertainty)
        return result


@dataclass
class Well:
    """The origin of a set of wellbores."""
    """The OSDU GroupType assigned to this resource object."""
    group_type: Any
    """The SRN which identifies this OSDU resource object without version."""
    id: str
    """The schema identification for the OSDU resource object following the pattern
    <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning
    scheme follows the semantic versioning, https://semver.org/.
    """
    kind: str
    """The entity's legal tags and compliance status."""
    legal: LegalMetaData
    """Indicates what kind of ownership Company has over data."""
    license_state: Any
    """Timestamp of the time at which Version 1 of this OSDU resource object was originated."""
    resource_object_creation_date_time: datetime
    """Timestamp of the time when the current version of this resource entered the OSDU."""
    resource_version_creation_date_time: datetime
    """The version number of this OSDU resource; set by the framework."""
    version: float
    """The access control tags associated with this entity."""
    acl: Optional[AccessControlList] = None
    """The links to data, which constitute the inputs."""
    ancestry: Optional[ParentList] = None
    data: Optional[Data] = None
    """Where does this data resource sit in the cradle-to-grave span of its existence?"""
    existence_kind: Optional[str] = None
    """The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions."""
    persistable_references: Optional[List[FrameOfReferenceMetaDataItem]] = None
    """Describes the current Curation status."""
    resource_curation_status: Optional[str] = None
    """The name of the home [cloud environment] region for this OSDU resource object."""
    resource_home_region_id: Optional[str] = None
    """The name of the host [cloud environment] region(s) for this OSDU resource object."""
    resource_host_region_i_ds: Optional[List[str]] = None
    """Describes the current Resource Lifecycle status."""
    resource_lifecycle_status: Optional[str] = None
    """Classifies the security level of the resource."""
    resource_security_classification: Optional[str] = None
    """Where did the data resource originate? This could be many kinds of entities, such as
    company, agency, team or individual.
    """
    source: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Well':
        assert isinstance(obj, dict)
        group_type = obj.get("GroupType")
        id = from_str(obj.get("ID"))
        kind = from_str(obj.get("Kind"))
        legal = LegalMetaData.from_dict(obj.get("Legal"))
        license_state = obj.get("LicenseState")
        resource_object_creation_date_time = from_datetime(obj.get("ResourceObjectCreationDateTime"))
        resource_version_creation_date_time = from_datetime(obj.get("ResourceVersionCreationDateTime"))
        version = from_float(obj.get("Version"))
        acl = from_union([AccessControlList.from_dict, from_none], obj.get("ACL"))
        ancestry = from_union([ParentList.from_dict, from_none], obj.get("Ancestry"))
        data = from_union([Data.from_dict, from_none], obj.get("Data"))
        existence_kind = from_union([from_str, from_none], obj.get("ExistenceKind"))
        persistable_references = from_union([lambda x: from_list(FrameOfReferenceMetaDataItem.from_dict, x), from_none], obj.get("PersistableReferences"))
        resource_curation_status = from_union([from_str, from_none], obj.get("ResourceCurationStatus"))
        resource_home_region_id = from_union([from_str, from_none], obj.get("ResourceHomeRegionID"))
        resource_host_region_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("ResourceHostRegionIDs"))
        resource_lifecycle_status = from_union([from_str, from_none], obj.get("ResourceLifecycleStatus"))
        resource_security_classification = from_union([from_str, from_none], obj.get("ResourceSecurityClassification"))
        source = from_union([from_str, from_none], obj.get("Source"))
        return Well(group_type, id, kind, legal, license_state, resource_object_creation_date_time, resource_version_creation_date_time, version, acl, ancestry, data, existence_kind, persistable_references, resource_curation_status, resource_home_region_id, resource_host_region_i_ds, resource_lifecycle_status, resource_security_classification, source)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GroupType"] = self.group_type
        result["ID"] = from_str(self.id)
        result["Kind"] = from_str(self.kind)
        result["Legal"] = to_class(LegalMetaData, self.legal)
        result["LicenseState"] = self.license_state
        result["ResourceObjectCreationDateTime"] = self.resource_object_creation_date_time.isoformat()
        result["ResourceVersionCreationDateTime"] = self.resource_version_creation_date_time.isoformat()
        result["Version"] = to_float(self.version)
        result["ACL"] = from_union([lambda x: to_class(AccessControlList, x), from_none], self.acl)
        result["Ancestry"] = from_union([lambda x: to_class(ParentList, x), from_none], self.ancestry)
        result["Data"] = from_union([lambda x: to_class(Data, x), from_none], self.data)
        result["ExistenceKind"] = from_union([from_str, from_none], self.existence_kind)
        result["PersistableReferences"] = from_union([lambda x: from_list(lambda x: to_class(FrameOfReferenceMetaDataItem, x), x), from_none], self.persistable_references)
        result["ResourceCurationStatus"] = from_union([from_str, from_none], self.resource_curation_status)
        result["ResourceHomeRegionID"] = from_union([from_str, from_none], self.resource_home_region_id)
        result["ResourceHostRegionIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.resource_host_region_i_ds)
        result["ResourceLifecycleStatus"] = from_union([from_str, from_none], self.resource_lifecycle_status)
        result["ResourceSecurityClassification"] = from_union([from_str, from_none], self.resource_security_classification)
        result["Source"] = from_union([from_str, from_none], self.source)
        return result


def well_from_dict(s: Any) -> Well:
    return Well.from_dict(s)


def well_to_dict(x: Well) -> Any:
    return to_class(Well, x)
