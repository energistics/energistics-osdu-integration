# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = file_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import List, Any, Optional, Dict, TypeVar, Callable, Type, cast
from enum import Enum
from datetime import datetime
import dateutil.parser


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return { k: f(v) for (k, v) in x.items() }


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


@dataclass
class AccessControlList:
    """The access control tags associated with this entity."""
    """The list of owners of this data record."""
    owners: List[str]
    """The list of viewers to which this data record is accessible/visible/discoverable."""
    viewers: List[str]

    @staticmethod
    def from_dict(obj: Any) -> 'AccessControlList':
        assert isinstance(obj, dict)
        owners = from_list(from_str, obj.get("Owners"))
        viewers = from_list(from_str, obj.get("Viewers"))
        return AccessControlList(owners, viewers)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Owners"] = from_list(from_str, self.owners)
        result["Viewers"] = from_list(from_str, self.viewers)
        return result


@dataclass
class ParentList:
    """The links to data, which constitute the inputs.
    
    A list of entity IDs in the data ecosystem, which act as legal parents to the current
    entity.
    """
    """An array of none, one or many entity references in the data ecosystem, which identify the
    source of data in the legal sense. Example: the 'parents' will be queried when e.g. the
    subscription of source data services is terminated; access to the derivatives is also
    terminated.
    """
    parents: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ParentList':
        assert isinstance(obj, dict)
        parents = from_union([lambda x: from_list(from_str, x), from_none], obj.get("Parents"))
        return ParentList(parents)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Parents"] = from_union([lambda x: from_list(from_str, x), from_none], self.parents)
        return result


class Endian(Enum):
    """Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will
    need to interpret from context indicators.
    """
    BIG = "BIG"
    LITTLE = "LITTLE"


class ScalarIndicator(Enum):
    """Enumerated string indicating whether to use the normal scalar field for scaling this
    field (STANDARD), no scaling (NOSCALE), or override scalar (OVERRIDE).  Default is
    current STANDARD (such as SEG-Y rev2).
    """
    NOSCALE = "NOSCALE"
    OVERRIDE = "OVERRIDE"
    STANDARD = "STANDARD"


@dataclass
class VectorHeaderMapping:
    """SRN of a reference value for a name of a property header such as INLINE, CDPX."""
    key_name: Optional[str] = None
    """Beginning byte position of header value, 1 indexed."""
    position: Optional[int] = None
    """Enumerated string indicating whether to use the normal scalar field for scaling this
    field (STANDARD), no scaling (NOSCALE), or override scalar (OVERRIDE).  Default is
    current STANDARD (such as SEG-Y rev2).
    """
    scalar_indicator: Optional[ScalarIndicator] = None
    """Scalar value (as defined by standard) when a value present in the header needs to be
    overwritten for this value.
    """
    scalar_override: Optional[float] = None
    """SRN to units of measure reference if header standard is not followed."""
    uo_m: Optional[str] = None
    """SRN of a reference value for binary data types, such as INT, UINT, FLOAT, IBM_FLOAT,
    ASCII, EBCDIC.
    """
    word_format: Optional[str] = None
    """Size of the word in bytes."""
    word_width: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'VectorHeaderMapping':
        assert isinstance(obj, dict)
        key_name = from_union([from_str, from_none], obj.get("KeyName"))
        position = from_union([from_int, from_none], obj.get("Position"))
        scalar_indicator = from_union([ScalarIndicator, from_none], obj.get("ScalarIndicator"))
        scalar_override = from_union([from_float, from_none], obj.get("ScalarOverride"))
        uo_m = from_union([from_str, from_none], obj.get("UoM"))
        word_format = from_union([from_str, from_none], obj.get("WordFormat"))
        word_width = from_union([from_int, from_none], obj.get("WordWidth"))
        return VectorHeaderMapping(key_name, position, scalar_indicator, scalar_override, uo_m, word_format, word_width)

    def to_dict(self) -> dict:
        result: dict = {}
        result["KeyName"] = from_union([from_str, from_none], self.key_name)
        result["Position"] = from_union([from_int, from_none], self.position)
        result["ScalarIndicator"] = from_union([lambda x: to_enum(ScalarIndicator, x), from_none], self.scalar_indicator)
        result["ScalarOverride"] = from_union([to_float, from_none], self.scalar_override)
        result["UoM"] = from_union([from_str, from_none], self.uo_m)
        result["WordFormat"] = from_union([from_str, from_none], self.word_format)
        result["WordWidth"] = from_union([from_int, from_none], self.word_width)
        return result


@dataclass
class Data:
    """MD5 checksum of file bytes - a 32 byte hexadecimal number"""
    checksum: Optional[str] = None
    """Number indicating degree of fidelity present in bulk data resulting from compression.
    Meaning of number depends on algorithm.
    """
    compression_level: Optional[float] = None
    """Name of a compression algorithm applied to the data as stored."""
    compression_method_type_id: Optional[str] = None
    """Encoding Format Type ID"""
    encoding_format_type_id: Optional[str] = None
    """Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will
    need to interpret from context indicators.
    """
    endian: Optional[Endian] = None
    """Length of file in bytes"""
    file_size: Optional[int] = None
    """URL or file path for the data in the file"""
    file_source: Optional[str] = None
    """Boolean that warns that an imperfect compression algorithm has been applied to the bulk
    binary data.  Details of the compression method need to be discovered from the format
    properties and file access methods.
    """
    lossy_compression_indicator: Optional[bool] = None
    """File system path to the data file as it existed before loading to the data platform"""
    pre_load_file_path: Optional[str] = None
    """Schema Format Type ID"""
    schema_format_type_id: Optional[str] = None
    """Array of objects which define the meaning and format of a tabular structure used in a
    binary file as a header.  The initial use case is the trace headers of a SEG-Y file.
    Note that some of this information may be repeated in the SEG-Y EBCDIC header.
    """
    vector_header_mapping: Optional[List[VectorHeaderMapping]] = None
    extension_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Data':
        assert isinstance(obj, dict)
        checksum = from_union([from_str, from_none], obj.get("Checksum"))
        compression_level = from_union([from_float, from_none], obj.get("CompressionLevel"))
        compression_method_type_id = from_union([from_str, from_none], obj.get("CompressionMethodTypeID"))
        encoding_format_type_id = from_union([from_str, from_none], obj.get("EncodingFormatTypeID"))
        endian = from_union([Endian, from_none], obj.get("Endian"))
        file_size = from_union([from_int, from_none], obj.get("FileSize"))
        file_source = from_union([from_str, from_none], obj.get("FileSource"))
        lossy_compression_indicator = from_union([from_bool, from_none], obj.get("LossyCompressionIndicator"))
        pre_load_file_path = from_union([from_str, from_none], obj.get("PreLoadFilePath"))
        schema_format_type_id = from_union([from_str, from_none], obj.get("SchemaFormatTypeID"))
        vector_header_mapping = from_union([lambda x: from_list(VectorHeaderMapping.from_dict, x), from_none], obj.get("VectorHeaderMapping"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        return Data(checksum, compression_level, compression_method_type_id, encoding_format_type_id, endian, file_size, file_source, lossy_compression_indicator, pre_load_file_path, schema_format_type_id, vector_header_mapping, extension_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Checksum"] = from_union([from_str, from_none], self.checksum)
        result["CompressionLevel"] = from_union([to_float, from_none], self.compression_level)
        result["CompressionMethodTypeID"] = from_union([from_str, from_none], self.compression_method_type_id)
        result["EncodingFormatTypeID"] = from_union([from_str, from_none], self.encoding_format_type_id)
        result["Endian"] = from_union([lambda x: to_enum(Endian, x), from_none], self.endian)
        result["FileSize"] = from_union([from_int, from_none], self.file_size)
        result["FileSource"] = from_union([from_str, from_none], self.file_source)
        result["LossyCompressionIndicator"] = from_union([from_bool, from_none], self.lossy_compression_indicator)
        result["PreLoadFilePath"] = from_union([from_str, from_none], self.pre_load_file_path)
        result["SchemaFormatTypeID"] = from_union([from_str, from_none], self.schema_format_type_id)
        result["VectorHeaderMapping"] = from_union([lambda x: from_list(lambda x: to_class(VectorHeaderMapping, x), x), from_none], self.vector_header_mapping)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        return result


@dataclass
class LegalMetaData:
    """The entity's legal tags and compliance status.
    
    Legal meta data like legal tags, relevant other countries, legal status.
    """
    """The list of legal tags."""
    legal_tags: List[str]
    """The list of other relevant data countries."""
    other_relevant_data_countries: List[str]
    """The legal status."""
    status: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LegalMetaData':
        assert isinstance(obj, dict)
        legal_tags = from_list(from_str, obj.get("LegalTags"))
        other_relevant_data_countries = from_list(from_str, obj.get("OtherRelevantDataCountries"))
        status = from_union([from_str, from_none], obj.get("Status"))
        return LegalMetaData(legal_tags, other_relevant_data_countries, status)

    def to_dict(self) -> dict:
        result: dict = {}
        result["LegalTags"] = from_list(from_str, self.legal_tags)
        result["OtherRelevantDataCountries"] = from_list(from_str, self.other_relevant_data_countries)
        result["Status"] = from_union([from_str, from_none], self.status)
        return result


class ReferenceKind(Enum):
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    AZIMUTH_REFERENCE = "AzimuthReference"
    CRS = "CRS"
    DATE_TIME = "DateTime"
    MEASUREMENT = "Measurement"
    UNIT = "Unit"


@dataclass
class FrameOfReferenceMetaDataItem:
    """A meta data item, which allows the association of named properties or property values to
    a Unit/Measurement/CRS/Azimuth/Time context.
    """
    """The kind of reference, unit, measurement, CRS or azimuth reference."""
    kind: ReferenceKind
    """The persistable reference string uniquely identifying the CRS or Unit."""
    persistable_reference: str
    """The name of the CRS or the symbol/name of the unit."""
    name: Optional[str] = None
    """The list of property names, to which this meta data item provides Unit/CRS context to.
    Data structures, which come in a single frame of reference, can register the property
    name, others require a full path like "data.structureA.propertyB" to define a unique
    context.
    """
    property_names: Optional[List[str]] = None
    """The list of property values, to which this meta data item provides Unit/CRS context to.
    Typically a unit symbol is a value to a data structure; this symbol is then registered in
    this propertyValues array and the persistableReference provides the absolute reference.
    """
    property_values: Optional[List[str]] = None
    """The uncertainty of the values measured given the unit or CRS unit."""
    uncertainty: Optional[float] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FrameOfReferenceMetaDataItem':
        assert isinstance(obj, dict)
        kind = ReferenceKind(obj.get("Kind"))
        persistable_reference = from_str(obj.get("PersistableReference"))
        name = from_union([from_str, from_none], obj.get("Name"))
        property_names = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyNames"))
        property_values = from_union([lambda x: from_list(from_str, x), from_none], obj.get("PropertyValues"))
        uncertainty = from_union([from_float, from_none], obj.get("Uncertainty"))
        return FrameOfReferenceMetaDataItem(kind, persistable_reference, name, property_names, property_values, uncertainty)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Kind"] = to_enum(ReferenceKind, self.kind)
        result["PersistableReference"] = from_str(self.persistable_reference)
        result["Name"] = from_union([from_str, from_none], self.name)
        result["PropertyNames"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_names)
        result["PropertyValues"] = from_union([lambda x: from_list(from_str, x), from_none], self.property_values)
        result["Uncertainty"] = from_union([to_float, from_none], self.uncertainty)
        return result


@dataclass
class File:
    """The generic file entity."""
    """The OSDU GroupType assigned to this resource object."""
    group_type: Any
    """The SRN which identifies this OSDU resource object without version."""
    id: str
    """The schema identification for the OSDU resource object following the pattern
    <Namespace>:<Source>:<Type>:<VersionMajor>.<VersionMinor>.<VersionPatch>. The versioning
    scheme follows the semantic versioning, https://semver.org/.
    """
    kind: str
    """The entity's legal tags and compliance status."""
    legal: LegalMetaData
    """Indicates what kind of ownership Company has over data."""
    license_state: Any
    """Timestamp of the time at which Version 1 of this OSDU resource object was originated."""
    resource_object_creation_date_time: datetime
    """Timestamp of the time when the current version of this resource entered the OSDU."""
    resource_version_creation_date_time: datetime
    """The version number of this OSDU resource; set by the framework."""
    version: float
    """The access control tags associated with this entity."""
    acl: Optional[AccessControlList] = None
    """The links to data, which constitute the inputs."""
    ancestry: Optional[ParentList] = None
    data: Optional[Data] = None
    """Where does this data resource sit in the cradle-to-grave span of its existence?"""
    existence_kind: Optional[str] = None
    """The meta data section linking the 'unitKey', 'crsKey' to self-contained definitions."""
    persistable_references: Optional[List[FrameOfReferenceMetaDataItem]] = None
    """Describes the current Curation status."""
    resource_curation_status: Optional[str] = None
    """The name of the home [cloud environment] region for this OSDU resource object."""
    resource_home_region_id: Optional[str] = None
    """The name of the host [cloud environment] region(s) for this OSDU resource object."""
    resource_host_region_i_ds: Optional[List[str]] = None
    """Describes the current Resource Lifecycle status."""
    resource_lifecycle_status: Optional[str] = None
    """Classifies the security level of the resource."""
    resource_security_classification: Optional[str] = None
    """Where did the data resource originate? This could be many kinds of entities, such as
    company, agency, team or individual.
    """
    source: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'File':
        assert isinstance(obj, dict)
        group_type = obj.get("GroupType")
        id = from_str(obj.get("ID"))
        kind = from_str(obj.get("Kind"))
        legal = LegalMetaData.from_dict(obj.get("Legal"))
        license_state = obj.get("LicenseState")
        resource_object_creation_date_time = from_datetime(obj.get("ResourceObjectCreationDateTime"))
        resource_version_creation_date_time = from_datetime(obj.get("ResourceVersionCreationDateTime"))
        version = from_float(obj.get("Version"))
        acl = from_union([AccessControlList.from_dict, from_none], obj.get("ACL"))
        ancestry = from_union([ParentList.from_dict, from_none], obj.get("Ancestry"))
        data = from_union([Data.from_dict, from_none], obj.get("Data"))
        existence_kind = from_union([from_str, from_none], obj.get("ExistenceKind"))
        persistable_references = from_union([lambda x: from_list(FrameOfReferenceMetaDataItem.from_dict, x), from_none], obj.get("PersistableReferences"))
        resource_curation_status = from_union([from_str, from_none], obj.get("ResourceCurationStatus"))
        resource_home_region_id = from_union([from_str, from_none], obj.get("ResourceHomeRegionID"))
        resource_host_region_i_ds = from_union([lambda x: from_list(from_str, x), from_none], obj.get("ResourceHostRegionIDs"))
        resource_lifecycle_status = from_union([from_str, from_none], obj.get("ResourceLifecycleStatus"))
        resource_security_classification = from_union([from_str, from_none], obj.get("ResourceSecurityClassification"))
        source = from_union([from_str, from_none], obj.get("Source"))
        return File(group_type, id, kind, legal, license_state, resource_object_creation_date_time, resource_version_creation_date_time, version, acl, ancestry, data, existence_kind, persistable_references, resource_curation_status, resource_home_region_id, resource_host_region_i_ds, resource_lifecycle_status, resource_security_classification, source)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GroupType"] = self.group_type
        result["ID"] = from_str(self.id)
        result["Kind"] = from_str(self.kind)
        result["Legal"] = to_class(LegalMetaData, self.legal)
        result["LicenseState"] = self.license_state
        result["ResourceObjectCreationDateTime"] = self.resource_object_creation_date_time.isoformat()
        result["ResourceVersionCreationDateTime"] = self.resource_version_creation_date_time.isoformat()
        result["Version"] = to_float(self.version)
        result["ACL"] = from_union([lambda x: to_class(AccessControlList, x), from_none], self.acl)
        result["Ancestry"] = from_union([lambda x: to_class(ParentList, x), from_none], self.ancestry)
        result["Data"] = from_union([lambda x: to_class(Data, x), from_none], self.data)
        result["ExistenceKind"] = from_union([from_str, from_none], self.existence_kind)
        result["PersistableReferences"] = from_union([lambda x: from_list(lambda x: to_class(FrameOfReferenceMetaDataItem, x), x), from_none], self.persistable_references)
        result["ResourceCurationStatus"] = from_union([from_str, from_none], self.resource_curation_status)
        result["ResourceHomeRegionID"] = from_union([from_str, from_none], self.resource_home_region_id)
        result["ResourceHostRegionIDs"] = from_union([lambda x: from_list(from_str, x), from_none], self.resource_host_region_i_ds)
        result["ResourceLifecycleStatus"] = from_union([from_str, from_none], self.resource_lifecycle_status)
        result["ResourceSecurityClassification"] = from_union([from_str, from_none], self.resource_security_classification)
        result["Source"] = from_union([from_str, from_none], self.source)
        return result


def file_from_dict(s: Any) -> File:
    return File.from_dict(s)


def file_to_dict(x: File) -> Any:
    return to_class(File, x)
