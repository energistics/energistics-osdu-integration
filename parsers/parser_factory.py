from zipfile import ZipFile
from .no_wpc_parser import NoWPCParser
from .parser import Parser
from .OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData
from typing import Dict, Type

from .resqml_2_0.default_parser import DefaultResqmlParser20
from .resqml_2_0.wellboretrajectoryrepresentation_parser import WellboreTrajectoryRepresentationParser20
from .resqml_2_0.wellboremarkerframerepresentation_parser import WellboreMarkerFrameRepresentationParser20
from .resqml_2_0.wellboreframerepresentation_parser import WellboreFrameRepresentationParser20
from .resqml_2_0.wellboreinterpretation_parser import WellboreInterpretationParser20
from .resqml_2_0.grid2drepresentation_parser import Grid2dRepresentationParser20


class ParserFactory:
    """
    Factory creating parsers from file type
    """

    def __init__(self, acl: AccessControlList, legal: LegalMetaData):
        self.acl = acl
        self.legal = legal
        self._parsers: Dict[str,Type[Parser]] = {}
        self.register_parser("application/x-resqml+xml;version=2.0", DefaultResqmlParser20)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreTrajectoryRepresentation",
                                WellboreTrajectoryRepresentationParser20)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreFrameRepresentation",
                                WellboreFrameRepresentationParser20)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreMarkerFrameRepresentation",
                                WellboreMarkerFrameRepresentationParser20)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_WellboreInterpretation",
                                WellboreInterpretationParser20)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_CategoricalProperty", NoWPCParser)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_ContinuousProperty", NoWPCParser)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_DiscreteProperty", NoWPCParser)
        self.register_parser("application/x-resqml+xml;version=2.0;type=obj_Grid2dRepresentation", Grid2dRepresentationParser20)


    def register_parser(self, key: str, parser: Type[Parser]):
        '''
        Associate a parser to a XML type name
        '''
        self._parsers[key] = parser

    def get_parser(self, xml_type: str, zip_file: ZipFile) -> Parser:
        '''
        Create a new parser based on a XML type
        '''
        parser = self._parsers.get(xml_type)
        if parser is None:
            if xml_type.startswith("application/x-resqml+xml;version=2.0") :
                return DefaultResqmlParser20(zip_file,self.acl,self.legal)
            else:
                return NoWPCParser(zip_file,self.acl,self.legal)
        return parser(zip_file,self.acl,self.legal)