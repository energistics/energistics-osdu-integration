from .parser_factory import ParserFactory
from dataclasses import dataclass

import logging

from openpack.basepack import Package

from collections import OrderedDict
from json import dumps


from osdu_api.base_client import BaseClient
from osdu_api.model.http_method import HttpMethod

from .OSDU_types.File_1_0_0 import File
from .OSDU_types.Document_1_0_0 import AccessControlList, LegalMetaData

from typing import Dict, List, Tuple, List, Optional

from zipfile import ZipFile

log = logging.getLogger(__name__)

def clean_empty(d) :
    '''
    Remove empty elements of a dictionary or a list of dictionary, recursively.
    '''
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v is not None]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v is not None}

@dataclass
class FileInfo:
    """ The path to the file """
    file_name: str
    """The list of owners of this data record."""
    owners: List[str]
    """The list of viewers to which this data record is accessible/visible/discoverable."""
    viewers: List[str]
    """The list of legal tags."""
    legal_tags: List[str]
    """The list of other relevant data countries."""
    other_relevant_data_countries: List[str]
    """The legal status."""
    status: Optional[str] = None


def parse_epc_file(info: FileInfo) ->Tuple[List[Dict],Dict,List[Dict]]:  
    '''
    Get the components of a manifest from an EPC file, each element is as dictionary or a list of dictionary:
    The tuple is made of: List of Files, WorkProduct, List of WorkProductComponents,
    Each part of the EPC document will instantiate the parser for its type.
    Parsers will create the WPC corresponding to each part, or a default WPC if the type is not declared.
    Each parser needs to be registered in parser_factory.py and derives from parser.py
    '''
    file_name = info.file_name
    zip_file = ZipFile(file_name)
    log.info(zip_file)

    pkg = Package()
    pkg._load_content_types(zip_file.read('[Content_Types].xml'))

    acl = AccessControlList(
        owners = info.owners,
        viewers = info.viewers
    )
    legal = LegalMetaData(
        legal_tags = info.legal_tags,
        other_relevant_data_countries = info.other_relevant_data_countries,
        status = info.status
    )
    parser_factory = ParserFactory(acl,legal)

    default_parser = parser_factory.get_parser("", zip_file)

    # Create workProduct section from file core properties
    work_product = default_parser.create_workproduct(file_name)

    # Create list of workProductComponents section
    wpc_dictionaries: List[Dict] = []
    for item in zip_file.namelist():
        if item.startswith('_rels') or item.startswith('[Content_Types]') or item.startswith('docProps'):
            continue

        # Get object type
        ct = pkg.content_types.find_for('/'+item)
        if ct == None:
            continue

        # Build workProductComponents
        parser = parser_factory.get_parser(ct.name, zip_file)

        wpc_s = parser.add_workproduct_components(item)
        for wpc in wpc_s:
            wpc_dictionaries += [clean_empty(wpc.to_dict())]
            
        log.info('Processed part %s %s', item, ct.name)

    # Create Files section
    # TODO HDF5 file
    current_file = default_parser.create_file_part(file_name)
    
    log.info('EPC file: %s processed', file_name)
    return [clean_empty(current_file.to_dict())], clean_empty(work_product.to_dict()), wpc_dictionaries


def create_epc_file_manifest(file: FileInfo):
    '''
    Create an OSDU manifest from an Energistics Packaging Convention file.
    Each part of the EPC document will instantiate the parser for its type.
    Parsers will create the WPC corresponding to each part, or a default WPC if the type is not declared.
    Each parser needs to be registered in parser_factory.py and derives from parser.py
    '''

    files, wp, wpc_list = parse_epc_file(file)

    # Create manifest file
    manifest = {
        "WorkProduct" : wp,
        "WorkProductComponents" : wpc_list,
        "Files" : files
    }

    out = dumps(manifest, indent=1)
    manifest_file = open(file.file_name+".manifest.json", "w")
    manifest_file.write(out)
    manifest_file.close()



def parse_epc_file_to_document_db(info: FileInfo):
    '''
    Fill OSDU DocumentDB from an Energistics Packaging Convention file.
    Each part of the EPC document will instantiate the parser for its type.
    Parsers will create the WPC corresponding to each part, or a default WPC if the type is not declared.
    Each parser needs to be registered in parser_factory.py and derives from parser.py
    filename is the name of the EPC file, while aclJson is a json string containing acl and legal information.
    '''

    files, wp, wpc_list = parse_epc_file(info)

    all_dictionaries = files
    all_dictionaries += wpc_list
    all_dictionaries += [wp]

    out = dumps(all_dictionaries, indent=1)

    # send all content in same request
    client = BaseClient()
    response = client.make_request(method=HttpMethod.PUT,url=client.storage_url,data=out)
    log.info('Response %s %s', response.status_code, response.text)

    log.info('EPC file: %s sent to document db', info.file_name)