cd ./Generated/work-product
quicktype --src WorkProduct.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t WorkProduct -o ../../parsers/OSDU_types/WorkProduct_1_0_0.py
cd ../master-data 
quicktype --src Well.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t Well -o ../../parsers/OSDU_types/Well_1_0_0.py
quicktype --src Wellbore.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t Wellbore -o ../../parsers/OSDU_types/Wellbore_1_0_0.py
quicktype --src Seismic3DInterpretationProject.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t Seismic3DInterpretationProject -o ../../parsers/OSDU_types/Seismic3DInterpretationProject_1_0_0.py
quicktype --src Seismic2DInterpretationProject.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t Seismic2DInterpretationProject -o ../../parsers/OSDU_types/Seismic2DInterpretationProject_1_0_0.py
cd ../work-product-component
quicktype --src Document.1.0.0.json -s schema --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t Document -o ../../parsers/OSDU_types/Document_1_0_0.py
quicktype --src WellboreTrajectory.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t WellboreTrajectory -o ../../parsers/OSDU_types/WellboreTrajectory_1_0_0.py
quicktype --src WellLog.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t WellLog -o ../../parsers/OSDU_types/WellLog_1_0_0.py
quicktype --src WellboreMarker.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t WellboreMarker -o ../../parsers/OSDU_types/WellboreMarker_1_0_0.py
quicktype --src SeismicBinGrid.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t SeismicBinGrid -o ../../parsers/OSDU_types/SeismicBinGrid_1_0_0.py
quicktype --src SeismicHorizon.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t SeismicHorizon -o ../../parsers/OSDU_types/SeismicHorizon_1_0_0.py
cd ../file
quicktype --src File.1.0.0.json --lang python --python-version 3.7 --no-ignore-json-refs -s schema -t File -o ../../parsers/OSDU_types/File_1_0_0.py