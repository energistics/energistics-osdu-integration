# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = work_product_load_manifest_staged_files_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Any, Optional, Dict, List, TypeVar, Callable, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_dict(f: Callable[[Any], T], x: Any) -> Dict[str, T]:
    assert isinstance(x, dict)
    return { k: f(v) for (k, v) in x.items() }


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


@dataclass
class GroupTypeProperties:
    pre_load_file_path: str

    @staticmethod
    def from_dict(obj: Any) -> 'GroupTypeProperties':
        assert isinstance(obj, dict)
        pre_load_file_path = from_str(obj.get("PreLoadFilePath"))
        return GroupTypeProperties(pre_load_file_path)

    def to_dict(self) -> dict:
        result: dict = {}
        result["PreLoadFilePath"] = from_str(self.pre_load_file_path)
        return result


@dataclass
class FileData:
    group_type_properties: GroupTypeProperties
    extension_properties: Optional[Dict[str, Any]] = None
    individual_type_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FileData':
        assert isinstance(obj, dict)
        group_type_properties = GroupTypeProperties.from_dict(obj.get("GroupTypeProperties"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        individual_type_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("IndividualTypeProperties"))
        return FileData(group_type_properties, extension_properties, individual_type_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["GroupTypeProperties"] = to_class(GroupTypeProperties, self.group_type_properties)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        result["IndividualTypeProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.individual_type_properties)
        return result


@dataclass
class File:
    associative_id: str
    data: FileData
    resource_security_classification: str
    resource_type_id: str

    @staticmethod
    def from_dict(obj: Any) -> 'File':
        assert isinstance(obj, dict)
        associative_id = from_str(obj.get("AssociativeID"))
        data = FileData.from_dict(obj.get("Data"))
        resource_security_classification = from_str(obj.get("ResourceSecurityClassification"))
        resource_type_id = from_str(obj.get("ResourceTypeID"))
        return File(associative_id, data, resource_security_classification, resource_type_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["AssociativeID"] = from_str(self.associative_id)
        result["Data"] = to_class(FileData, self.data)
        result["ResourceSecurityClassification"] = from_str(self.resource_security_classification)
        result["ResourceTypeID"] = from_str(self.resource_type_id)
        return result


@dataclass
class PurpleIndividualTypeProperties:
    description: str
    name: str

    @staticmethod
    def from_dict(obj: Any) -> 'PurpleIndividualTypeProperties':
        assert isinstance(obj, dict)
        description = from_str(obj.get("Description"))
        name = from_str(obj.get("Name"))
        return PurpleIndividualTypeProperties(description, name)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Description"] = from_str(self.description)
        result["Name"] = from_str(self.name)
        return result


@dataclass
class WorkProductData:
    individual_type_properties: PurpleIndividualTypeProperties
    extension_properties: Optional[Dict[str, Any]] = None
    group_type_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProductData':
        assert isinstance(obj, dict)
        individual_type_properties = PurpleIndividualTypeProperties.from_dict(obj.get("IndividualTypeProperties"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        group_type_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("GroupTypeProperties"))
        return WorkProductData(individual_type_properties, extension_properties, group_type_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["IndividualTypeProperties"] = to_class(PurpleIndividualTypeProperties, self.individual_type_properties)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        result["GroupTypeProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.group_type_properties)
        return result


@dataclass
class WorkProduct:
    components_associative_i_ds: List[str]
    data: WorkProductData
    resource_security_classification: str
    resource_type_id: str

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProduct':
        assert isinstance(obj, dict)
        components_associative_i_ds = from_list(from_str, obj.get("ComponentsAssociativeIDs"))
        data = WorkProductData.from_dict(obj.get("Data"))
        resource_security_classification = from_str(obj.get("ResourceSecurityClassification"))
        resource_type_id = from_str(obj.get("ResourceTypeID"))
        return WorkProduct(components_associative_i_ds, data, resource_security_classification, resource_type_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ComponentsAssociativeIDs"] = from_list(from_str, self.components_associative_i_ds)
        result["Data"] = to_class(WorkProductData, self.data)
        result["ResourceSecurityClassification"] = from_str(self.resource_security_classification)
        result["ResourceTypeID"] = from_str(self.resource_type_id)
        return result


@dataclass
class FluffyIndividualTypeProperties:
    description: str
    name: str

    @staticmethod
    def from_dict(obj: Any) -> 'FluffyIndividualTypeProperties':
        assert isinstance(obj, dict)
        description = from_str(obj.get("Description"))
        name = from_str(obj.get("Name"))
        return FluffyIndividualTypeProperties(description, name)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Description"] = from_str(self.description)
        result["Name"] = from_str(self.name)
        return result


@dataclass
class WorkProductComponentData:
    individual_type_properties: FluffyIndividualTypeProperties
    extension_properties: Optional[Dict[str, Any]] = None
    group_type_properties: Optional[Dict[str, Any]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProductComponentData':
        assert isinstance(obj, dict)
        individual_type_properties = FluffyIndividualTypeProperties.from_dict(obj.get("IndividualTypeProperties"))
        extension_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("ExtensionProperties"))
        group_type_properties = from_union([lambda x: from_dict(lambda x: x, x), from_none], obj.get("GroupTypeProperties"))
        return WorkProductComponentData(individual_type_properties, extension_properties, group_type_properties)

    def to_dict(self) -> dict:
        result: dict = {}
        result["IndividualTypeProperties"] = to_class(FluffyIndividualTypeProperties, self.individual_type_properties)
        result["ExtensionProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.extension_properties)
        result["GroupTypeProperties"] = from_union([lambda x: from_dict(lambda x: x, x), from_none], self.group_type_properties)
        return result


@dataclass
class WorkProductComponent:
    associative_id: str
    data: WorkProductComponentData
    file_associative_i_ds: List[str]
    resource_security_classification: str
    resource_type_id: str

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProductComponent':
        assert isinstance(obj, dict)
        associative_id = from_str(obj.get("AssociativeID"))
        data = WorkProductComponentData.from_dict(obj.get("Data"))
        file_associative_i_ds = from_list(from_str, obj.get("FileAssociativeIDs"))
        resource_security_classification = from_str(obj.get("ResourceSecurityClassification"))
        resource_type_id = from_str(obj.get("ResourceTypeID"))
        return WorkProductComponent(associative_id, data, file_associative_i_ds, resource_security_classification, resource_type_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["AssociativeID"] = from_str(self.associative_id)
        result["Data"] = to_class(WorkProductComponentData, self.data)
        result["FileAssociativeIDs"] = from_list(from_str, self.file_associative_i_ds)
        result["ResourceSecurityClassification"] = from_str(self.resource_security_classification)
        result["ResourceTypeID"] = from_str(self.resource_type_id)
        return result


@dataclass
class WorkProductLoadManifestStagedFiles:
    files: List[File]
    work_product: WorkProduct
    work_product_components: List[WorkProductComponent]

    @staticmethod
    def from_dict(obj: Any) -> 'WorkProductLoadManifestStagedFiles':
        assert isinstance(obj, dict)
        files = from_list(File.from_dict, obj.get("Files"))
        work_product = WorkProduct.from_dict(obj.get("WorkProduct"))
        work_product_components = from_list(WorkProductComponent.from_dict, obj.get("WorkProductComponents"))
        return WorkProductLoadManifestStagedFiles(files, work_product, work_product_components)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Files"] = from_list(lambda x: to_class(File, x), self.files)
        result["WorkProduct"] = to_class(WorkProduct, self.work_product)
        result["WorkProductComponents"] = from_list(lambda x: to_class(WorkProductComponent, x), self.work_product_components)
        return result


def work_product_load_manifest_staged_files_from_dict(s: Any) -> WorkProductLoadManifestStagedFiles:
    return WorkProductLoadManifestStagedFiles.from_dict(s)


def work_product_load_manifest_staged_files_to_dict(x: WorkProductLoadManifestStagedFiles) -> Any:
    return to_class(WorkProductLoadManifestStagedFiles, x)
