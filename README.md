
# Package to interface Energistics files with OSDU schemas and services

## Parsers library

The parsers directory contains code allowing to create OSDU standard JSON documents from Energistics files.
The input files must follow either the Energistics XML convention or Energistics Packaging convention.

For an EPC file:
- The two main methods create_epc_file_manifest and parse_epc_file_to_documentdb will respectively create a manifest file and send the documents to an OSDU DocumentDB.
- This code is parsing the content of an EPC files and invoking for each part an individual parser according to their xml type. Parsers are instantiated through a factory, and are responsible for providing JSON parts for OSDU WorkProduct, WorkProductComponent and File. The base class for parsers contains all the necessary method for EPC manipulation.

## How to use it

### Dependencies

- Dependencies

```shell
python -m pip install h5py python-dateutil lxml xmljson openpack pyyaml requests
```

- It also depends on the osdu_api library that can be found in the `..\os-python-sdk` directory. Follow corresponding [README](..\os-python-sdk\README.md) on how to install it.

### To create package and install locally

- Make sure you have setuptools and wheel installed.

```shell
python -m pip install --user --upgrade setuptools wheel
```

- Run

```shell
python setup.py sdist bdist_wheel
```

- Make sure osdu_energistics_parsers isn't already installed

```shell
python -m pip uninstall osdu_energistics_parsers
```

- Run

```shell
python -m pip install <YOUR PATH TO PACKAGE>/dist/osdu_energistics_parsers-0.0.1-py3-none-any.whl
```

 make sure to substitute your machine's path in that command.

## Example and tests

### Example python import once library installed

```python
from osdu_energistics_parsers.storage.record_client import RecordClient
```

The file resqml_example.py indicates how to create a R2 OSDU manifest or populate the R3 OSDU documentDB and test_resqml_parsers.py provide unit tests for the package.
The file test_resqml_parsers.py is used to unit-test the library.

## Code organization

This library relies on python class generation from two types of schemas:

- The OSDU schemas are transformed into classes using quicktype (See ./create_types_from_OSDU_schema.bat). The OSDU json schemas located in Generated folder are used to create classes in parsers/OSDU_types
- The individual Energistics XML schemas are transformed into python classes using https://pypi.org/project/generateDS and the resulting file is located with the parsers for this schema version (example parsers/resqml_2_0_xsd)

The code directly inside the parsers directory, correspond to the code common to all Energistics ML, while the subdirectories inside it contains schema specific code.
A factory mechanism is used to associate the MIME type of the file (or file part), to the corresponding parsers.
It is the responsibility of each individual parser, to make the translation from Energistics classes to OSDU classes.